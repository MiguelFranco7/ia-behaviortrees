#include "stdafx.h"
#include "SeekSteering.h"
#include "character.h"

SeekSteering::SeekSteering(Character *character, USVec2D target) {
	mCharacter = character;
	mTarget    = target;
}

USVec2D SeekSteering::GetSteering() {
	
	mDreamVelocity = mTarget - mCharacter->GetLoc();
	mDreamVelocity.NormSafe();
	mDreamVelocity.Scale(mCharacter->GetParams().max_velocity);

	mAcce = mDreamVelocity - mCharacter->GetLinearVelocity();
	mAcce.NormSafe();
	mAcce.Scale(mCharacter->GetParams().max_acceleration);
	
	return mAcce;
}

void SeekSteering::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);

	// Vector (L�nea) de la velocidad deseada calculada en el frame anterior
	MOAIDraw::DrawLine(USVec2D(mCharacter->GetLoc().mX, mCharacter->GetLoc().mY), mDreamVelocity);

	gfxDevice.SetPenColor(0.0f, 1.0f, 0.0f, 0.5f);
	// Vector (L�nea) de la aceleraci�n calculada en el frame anterior 
	MOAIDraw::DrawLine(USVec2D(mCharacter->GetLoc().mX, mCharacter->GetLoc().mY), mAcce);
}
