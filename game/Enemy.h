#pragma once

#include "character.h"

class StateMachine;

class Enemy : public Character {
public:
	DECL_LUA_FACTORY(Enemy)

	Enemy();
	~Enemy();

	virtual void DrawDebug();

	Character * GetPlayerTarget() { return mPlayerTarget; }
	void	    SetPlayerTarget(Character * character) { mPlayerTarget = character; }

	bool GetIsAttacking();
	void SetIsAttacking(bool isAttacking);

	void MoveToPlayer();

protected:
	virtual void OnStart();
	virtual void OnStop();
	virtual void OnUpdate(float step);

private:
	static Character *mPlayerTarget;
	bool			  mIsAttacking;

	virtual void InitStateMachine();
	virtual void InitBehaviorTree();


	// Lua configuration
public:
	virtual void RegisterLuaFuncs(MOAILuaState& state);

private:
	static int _setLinearVel(lua_State* L);
	static int _setAngularVel(lua_State* L);
	static int _setTarget(lua_State* L);
};