#include <stdafx.h>
#include "character.h"
#include "pathfinding\pathfinder.h"
#include <tinyxml.h>
#include <params.h>

Character::Character() : mLinearVelocity(0.0f, 0.0f), mAngularVelocity(0.0f)
{
	RTTI_BEGIN
		RTTI_EXTEND(MOAIEntity2D)
	RTTI_END
}

Character::~Character() {
	// TODO: Complete
}

void Character::UpdatePath(vector<USVec2D> pathfollowing) {
	mPath		  = pathfollowing;
	pathFollowing = PathFollowingSteering(this, mPath);
}

void Character::OnStart() {
	ReadParams("sample/params.xml", mParams);
	// ReadPath("sample/path.xml", mPath);
	// ReadObstacles("sample/obstacles.xml", mObstacles);

	pathFollowing = PathFollowingSteering(this, mPath);
	//mTarget = mParams.targetPosition;

	mCurrentHealth = mMaxHealth;

	mDeltaTime = 0.f;
}

void Character::OnStop() {

}

void Character::OnUpdate(float step) {
	// Update mDeltaTime
	mDeltaTime = step;

	// PATH FOLLOWING
	USVec2D acce = pathFollowing.GetSteering();

	// OBSTACLE AVOIDANCE
	/*obstacleAvoidance = ObstacleAvoidanceSteering(this);
	USVec2D acceObs = obstacleAvoidance.GetSteering();*/

	mLinearVelocity.mX += acce.mX * step;// + acceObs.mX;
	mLinearVelocity.mY += acce.mY * step;// + acceObs.mY;
	SetLoc(GetLoc() += mLinearVelocity * step);

	// ALIGN TO MOVEMENT
	alignToMovement = AlignToMovementSteering(this);
	float acceRot = alignToMovement.GetSteering();

	mAngularVelocity += acceRot * step;
	SetRot(GetRot() + mAngularVelocity * step);
}

void Character::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(0.0f, 0.0f, 1.0f, 0.5f);

	//MOAIDraw::DrawPoint(0.0f, 0.0f);

	//seek.DrawDebug();
	//arrive.DrawDebug();
	pathFollowing.DrawDebug();
	//align.DrawDebug();
	//alignToMovement.DrawDebug();
	//obstacleAvoidance.DrawDebug();

	// Pintado del Path del character.
	if (mPath.size() > 0) {
		gfxDevice.SetPenColor(0.0f, 0.3f, 1.0f, 0.5f);
		for (int i = 0; i < mPath.size() - 1; i++) {
			MOAIDraw::DrawLine(mPath[i], mPath[i + 1]);
		}
	}

	// Pintado de los obstaculos.
	//gfxDevice.SetPenColor(1.0f, 1.0f, 1.0f, 1.0f);
	//for (int i = 0; i < mObstacles.size(); i++) {
	//	// Si tiene collision se pinta de rojo.
	//	if (mObstacles[i].collision)
	//		gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 1.0f);
	//	else
	//		gfxDevice.SetPenColor(1.0f, 1.0f, 1.0f, 1.0f);

	//	MOAIDraw::DrawEllipseFill(mObstacles[i].position.mX, mObstacles[i].position.mY, mObstacles[i].radius, mObstacles[i].radius, 100);
	//}

	for (int i = 0; i < mPath.size(); i++) {
		MOAIDraw::DrawEllipseFill(mPath[i].mX, mPath[i].mY, 15, 15, 100);
	}
}

void Character::OnDamage(float damage) {
	mCurrentHealth -= damage;
}

void Character::InitStateMachine() {}

void Character::InitBehaviorTree() {}


// Lua configuration

void Character::RegisterLuaFuncs(MOAILuaState& state) {
	MOAIEntity2D::RegisterLuaFuncs(state);
	
	luaL_Reg regTable [] = {
		{ "setLinearVel",			_setLinearVel},
		{ "setAngularVel",			_setAngularVel},
		{ NULL, NULL }
	};

	luaL_register(state, 0, regTable);
}

int Character::_setLinearVel(lua_State* L) {
	MOAI_LUA_SETUP(Character, "U")
	
	float pX = state.GetValue<float>(2, 0.0f);
	float pY = state.GetValue<float>(3, 0.0f);
	self->SetLinearVelocity(pX, pY);
	return 0;	
}

int Character::_setAngularVel(lua_State* L) {
	MOAI_LUA_SETUP(Character, "U")
	
	float angle = state.GetValue<float>(2, 0.0f);
	self->SetAngularVelocity(angle);

	return 0;
}
	