#include <stdafx.h>
#include "pathfinder.h"
#include "Player.h"
#include "Enemy.h"
#include <sstream>
#include <string>
#include <cmath>

Pathfinder::Pathfinder() : MOAIEntity2D() {
	RTTI_BEGIN
		RTTI_EXTEND(MOAIEntity2D)
		RTTI_END

	m_StartPosition = USVec2D(-435, -204);
	m_EndPosition   = USVec2D(-436, -162);
	
	ReadNav("sample/smnav.xml", m_NavMesh);
	
	CleanList();
}

Pathfinder::~Pathfinder() {

}

void Pathfinder::UpdatePath() {
	//AStar();
}

void Pathfinder::AStar() {
	CleanList();

	while (m_PolygonStart != -1 && m_PolygonEnd != -1 && !PathfindStep())
		cout << "Cargando Path" << endl;

	for (int i = 0; i < m_CharacterList.size(); i++) {
		m_CharacterList[i]->UpdatePath(m_Pathfollowing);
	}
}

NavPolygon* Pathfinder::SortedBox() {
	float sort     = INFINITY;
	int   position = 0;

	for (int i = 0; i < m_OpenList.size(); i++) {
		if (m_OpenList[i]->totalCost < sort) {
			sort = m_OpenList[i]->totalCost;
			position = i;
		}
	}

	NavPolygon *a = m_OpenList[position];

	m_OpenList.erase(m_OpenList.begin() + position);

	return a;
}

void Pathfinder::NextNode(NavPolygon *padre) {
	vector<NavPolygon*> NextList;

	float dist = 0.f;
	for (int i = 0; i < padre->mEdges.size(); i++) {	  // Recorre las aristas
		NavPolygon * hijo = padre->mEdges[i].mPNeighbour; // Guarda el posible siguiente

		if (hijo && padre->father != hijo && !IsClosed(hijo)) { // Comprueba que el siguiente sea valido
			USVec2D midPoint(0, 0);
			// Longitud de la arista i
			USVec2D dif = (padre->mVerts[padre->mEdges[i].mVerts[1]] - padre->mVerts[padre->mEdges[i].mVerts[0]]);
			// Guarda el punto medio de la arista i
			midPoint.mX = padre->mVerts[padre->mEdges[i].mVerts[0]].mX + dif.mX * 0.5f;
			midPoint.mY = padre->mVerts[padre->mEdges[i].mVerts[0]].mY + dif.mY * 0.5f;
			// Calcula la distancia al final
			dist        = sqrt((m_EndPosition.mX - midPoint.mX) * (m_EndPosition.mX - midPoint.mX) + (m_EndPosition.mY - midPoint.mY) * (m_EndPosition.mY - midPoint.mY));
			
			if (padre->father == nullptr) {
				hijo->cost = sqrt((midPoint.mX - m_StartPosition.mX) * (midPoint.mX - m_StartPosition.mX) + (midPoint.mY - m_StartPosition.mY) * (midPoint.mY - m_StartPosition.mY));
			} else {
				unsigned int sizeDad = padre->father->mEdges.size();
				unsigned int edgeId  = 0;

				for (int j = 0; j < sizeDad; j++) {
					if (padre->father->mEdges[j].mPNeighbour == padre) {
						edgeId = j;
						break;
					}
				}

				USVec2D midPointArrive(0, 0);
				USVec2D difArrive = (padre->father->mVerts[padre->father->mEdges[edgeId].mVerts[1]] - padre->father->mVerts[padre->father->mEdges[edgeId].mVerts[0]]);
				midPointArrive.mX = padre->father->mVerts[padre->father->mEdges[edgeId].mVerts[0]].mX + dif.mX * 0.5f;
				midPointArrive.mY = padre->father->mVerts[padre->father->mEdges[edgeId].mVerts[0]].mY + dif.mY * 0.5f;
				
				hijo->cost = sqrt((midPoint.mX - midPointArrive.mX) * (midPoint.mX - midPointArrive.mX) + (midPoint.mY - midPointArrive.mY) * (midPoint.mY - midPointArrive.mY));
			}

			if (hijo->actualCost >= padre->actualCost + hijo->cost) {
				hijo->actualCost = padre->actualCost + hijo->cost;
				hijo->costToEnd  = dist;
				hijo->father     = padre;
			}

			hijo->totalCost = hijo->actualCost + hijo->costToEnd;
			NextList.push_back(hijo);
		}
	}

	for (int i = 0; i < NextList.size(); i++) {
		if (IsPolygonInList(NextList[i], m_ClosedList) < 0) {
			int id_olist = IsPolygonInList(NextList[i], m_OpenList);

			if (id_olist < 0)
				m_OpenList.push_back(NextList[i]);
		}
	}
}

int Pathfinder::IsPolygonInList(const NavPolygon * polygon, vector<NavPolygon *> list) {
	unsigned int size = list.size();

	for (unsigned int i = 0; i < size; i++)
		if (list[i] == polygon)
			return i;

	return -1;
}

bool Pathfinder::IsClosed(NavPolygon * polygon) {
	bool isClosed = false;

	for (auto it = m_ClosedList.begin(); it != m_ClosedList.end(); it++) {
		if (polygon == *it) {
			isClosed = true;
			break;
		}
	}

	return isClosed;
}

void Pathfinder::CleanList() {
	m_OpenList   = {};
	m_ClosedList = {};
	m_FinishPath = {};

	for (int i = 0; i < m_NavMesh.size(); i++) {
		m_NavMesh[i].father = nullptr;
		m_NavMesh[i].actualCost = INFINITY;
		m_NavMesh[i].cost = 0;
		m_NavMesh[i].costToEnd = 0;
		m_NavMesh[i].totalCost = 0;
	}

	m_Pathfollowing.clear();

	m_IsFinishPath = false;

	m_PolygonStart = FindShortPolygon(m_StartPosition);
	m_PolygonEnd   = FindShortPolygon(m_EndPosition);

	if (m_PolygonStart != -1 && m_PolygonEnd != -1) {
		m_NavMesh[m_PolygonStart].costToEnd = (m_EndPosition - m_StartPosition).Length();

		m_OpenList.push_back(&m_NavMesh[m_PolygonStart]);
	}
}

void Pathfinder::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();

	// PINTAMOS LA NAVMESH GENERADA
	for (int i = 0; i < m_NavMesh.size(); i++) {
	gfxDevice.SetPenColor(0.0f, 1.0f, 0.0f, 0.5f);
	MOAIDraw::DrawPolygon(m_NavMesh[i].mVerts);

	gfxDevice.SetPenColor(0.0f, 0.0f, 1.0f, 0.2f);
	MOAIDraw::DrawPolygonFilled(m_NavMesh[i].mVerts);
	}

	// PINTAMOS INICIO Y FIN
	gfxDevice.SetPenColor(0.5f, 0.5f, 0.5f, 0.5f);
	MOAIDraw::DrawEllipseFill(m_StartPosition.mX, m_StartPosition.mY, 10, 10, 100);
	gfxDevice.SetPenColor(1.0f, 1.0f, 0.0f, 0.5f);
	MOAIDraw::DrawEllipseFill(m_EndPosition.mX, m_EndPosition.mY, 10, 10, 100);

	// Tama�o del cuadrado
	//float x = 1024 / 20;
	//float y = 768 / 15;

	// Pintamos el grid
	//gfxDevice.SetPenColor(1.0f, 0.3f, 0.0f, 0.5f);
	//for (int i = 0; i < 20; i++) {
	//	MOAIDraw::DrawLine(i*x - 512, 0 * y - 384, i*x - 512, 0 * y - 384 + 768);
	//}

	//for (int j = 0; j < 15; j++) {
	//	MOAIDraw::DrawLine(0 * x - 512, j * y - 384, 0 * x - 512 + 1024, j * y - 384);
	//}

	//// PINTADO DE LA LISTA CERRADA EN BLANCO
	//for (int i = 0; i < m_ClosedList.size(); i++) {
	//	gfxDevice.SetPenColor(1.0f, 1.0f, 1.0f, 0.5f);
	//	MOAIDraw::DrawEllipseFill(m_ClosedList[i]->pointPosition.mX, m_ClosedList[i]->pointPosition.mY, 5, 5, 100);
	//}

	//// PINTADO DE LA LISTA ABIERTA EN ROJO
	//for (int i = 0; i < m_OpenList.size(); i++) {
	//	gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);
	//	MOAIDraw::DrawEllipseFill(m_OpenList[i]->pointPosition.mX, m_OpenList[i]->pointPosition.mY, 5, 5, 100);
	//}

	// PINTAMOS EL PATH COMPLETO
	if (m_Pathfollowing.size() > 0) {
		for (int i = 0; i < m_Pathfollowing.size() - 1; i++) {
			USVec2D start = m_Pathfollowing[i];
			USVec2D end = start;
			if (i + 1 < m_Pathfollowing.size())
				end = m_Pathfollowing[i + 1];

			// PINTAMOS LA UNION ENTRE PUNTOS
			gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 1.f);
			MOAIDraw::DrawLine(start, end);

			// PINTAMOS LOS PUNTOS DEL PATH
			gfxDevice.SetPenColor(1.0f, 0.0f, 1.0f, 0.5f);
			MOAIDraw::DrawEllipseFill(m_Pathfollowing[i].mX, m_Pathfollowing[i].mY, 5, 5, 100);
		}
	}
}

bool Pathfinder::PathfindStep() {
	NavPolygon * actual = SortedBox();

	m_ClosedList.push_back(actual);

	if (FindPointInPolygon(m_EndPosition, actual)) {
		m_IsFinishPath = true;
		// Calcular path de inicio a final para pintar el camino
		cout << "Acabado" << endl;
		GenerateCompletePath(actual);
	} else {
		NextNode(actual);
	}

	return m_IsFinishPath;
}

int Pathfinder::FindShortPolygon(USVec2D position) {
	for (int i = 0; i < m_NavMesh.size(); i++) {
		if (FindPointInPolygon(position, &m_NavMesh[i]))
			return i;
	}

	return -1;
}

bool Pathfinder::FindPointInPolygon(USVec2D position, const NavPolygon * polygon) {
	int counter = 0;
	int i;
	double xinters;
	USVec2D p1, p2;
	int N = polygon->mVerts.size();
	
	p1 = polygon->mVerts[0];
	for (i = 1; i <= N; i++) {
		p2 = polygon->mVerts[i % N];
		if (position.mY > MIN(p1.mY, p2.mY)) {
			if (position.mY <= MAX(p1.mY, p2.mY)) {
				if (position.mX <= MAX(p1.mX, p2.mX)) {
					if (p1.mY != p2.mY) {
						xinters = (position.mY - p1.mY)*(p2.mX - p1.mX) / (p2.mY - p1.mY) + p1.mX;
						if (p1.mX == p2.mX || position.mX <= xinters)
							counter++;
					}
				}
			}
		}
		p1 = p2;
	}

	if (counter % 2 == 0)
		return false;
	else
		return true;
}

void Pathfinder::GenerateCompletePath(const NavPolygon * nodo) {
	m_Pathfollowing.clear();
	m_Pathfollowing.push_back(m_EndPosition);

	const NavPolygon * polygon = nodo;
	while (polygon) {
		if (FindPointInPolygon(m_StartPosition, polygon)) {
			polygon = nullptr;
		} else {
			if (IsPolygonInList(polygon->father, m_ClosedList) >= 0) {
				unsigned int numEdges = polygon->mEdges.size();
				int edgeId = -1;

				for (unsigned int i = 0; i < numEdges; i++) {
					if (polygon->mEdges[i].mPNeighbour == polygon->father) {
						edgeId = i;
						break;
					}
				}

				if (edgeId >= 0) {
					USVec2D midPoint(0, 0);
					USVec2D dif = (polygon->mVerts[polygon->mEdges[edgeId].mVerts[1]] - polygon->mVerts[polygon->mEdges[edgeId].mVerts[0]]);
					midPoint.mX = polygon->mVerts[polygon->mEdges[edgeId].mVerts[0]].mX + dif.mX * 0.5f;
					midPoint.mY = polygon->mVerts[polygon->mEdges[edgeId].mVerts[0]].mY + dif.mY * 0.5f;
					polygon     = polygon->father;

					m_Pathfollowing.push_back(midPoint);
				}
				else
					polygon = nullptr;
			} else
				polygon = nullptr;
		}
	}

	m_Pathfollowing.push_back(m_StartPosition);
	reverse(m_Pathfollowing.begin(), m_Pathfollowing.end());
}


//lua configuration ----------------------------------------------------------------//
void Pathfinder::RegisterLuaFuncs(MOAILuaState& state) {
	MOAIEntity::RegisterLuaFuncs(state);

	luaL_Reg regTable[] = {
		{ "setStartPosition",		_setStartPosition },
		{ "setEndPosition",			_setEndPosition },
		{ "pathfindStep",           _pathfindStep },
		{ "addCharacter",			_addCharacter },
		{ NULL, NULL }
	};

	luaL_register(state, 0, regTable);
}

int Pathfinder::_setStartPosition(lua_State* L) {
	MOAI_LUA_SETUP(Pathfinder, "U")

	float pX = state.GetValue<float>(2, 0.0f);
	float pY = state.GetValue<float>(3, 0.0f);
	self->SetStartPosition(pX, pY);
	//self->CleanList();
	self->AStar();
	return 0;
}

int Pathfinder::_setEndPosition(lua_State* L) {
	MOAI_LUA_SETUP(Pathfinder, "U")

	float pX = state.GetValue<float>(2, 0.0f);
	float pY = state.GetValue<float>(3, 0.0f);
	self->SetEndPosition(pX, pY);
	//self->CleanList();
	self->AStar();
	return 0;
}

int Pathfinder::_pathfindStep(lua_State* L) {
	MOAI_LUA_SETUP(Pathfinder, "U")

	//self->PathfindStep();
	return 0;
}

int Pathfinder::_addCharacter(lua_State* L) {
	MOAI_LUA_SETUP(Pathfinder, "U")

	Character * character = state.GetLuaObject<Character>(2, true);
	if (character)
		self->m_CharacterList.push_back(character);
	else {
		Player * player = state.GetLuaObject<Player>(2, true);

		if (player)
			self->m_CharacterList.push_back(player);
		else {
			Enemy * enemy = state.GetLuaObject<Enemy>(2, true);
			if (enemy)
				self->m_CharacterList.push_back(enemy);
		}
	}

	//self->m_CharacterList.push_back(state.GetLuaObject<Character>(2, true));
	return 0;
}
