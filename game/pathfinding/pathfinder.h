#ifndef __PATHFINDER_H__
#define __PATHFINDER_H__

#include <moaicore/MOAIEntity2D.h>
#include <fstream>
#include "params.h"

class Character;

class Pathfinder : public virtual MOAIEntity2D {
public:
	Pathfinder();
	~Pathfinder();

	virtual void DrawDebug();

	void		   SetStartPosition(float x, float y) { m_StartPosition = USVec2D(x, y); UpdatePath(); }
	void		   SetEndPosition(float x, float y) { m_EndPosition = USVec2D(x, y); UpdatePath(); }
	const USVec2D& GetStartPosition() const { return m_StartPosition; }
	const USVec2D& GetEndPosition() const { return m_EndPosition; }

	bool PathfindStep();
	vector<USVec2D> GetPathfollowing() { return m_Pathfollowing; }

private:
	void		 UpdatePath();
	void		 AStar();
	NavPolygon * SortedBox();
	void	     NextNode(NavPolygon *padre);
	int			 IsPolygonInList(const NavPolygon * polygon, vector<NavPolygon *> list);
	bool	     IsClosed(NavPolygon * polygon);
	void	     CleanList(); // Funci�n que inicia las listas al cambiar de inicio o fin.
	int		     FindShortPolygon(USVec2D position);
	bool		 FindPointInPolygon(USVec2D position, const NavPolygon * polygon);
	void		 GenerateCompletePath(const NavPolygon * nodo);

	USVec2D m_StartPosition;
	USVec2D m_EndPosition;
	int     m_PolygonStart;
	int		m_PolygonEnd;

	vector<NavPolygon>		m_NavMesh;
	vector<NavPolygon*>		m_OpenList;
	vector<NavPolygon*>		m_ClosedList;
	bool					m_IsFinishPath;
	vector<NavPolygon*>		m_FinishPath;
	NavPolygon            * m_FinishPolygon;
	vector<USVec2D>			m_Pathfollowing;

	vector<Character *>		m_CharacterList;

	// Lua configuration
public:
	DECL_LUA_FACTORY(Pathfinder)
public:
	virtual void RegisterLuaFuncs(MOAILuaState& state);
private:
	static int _setStartPosition(lua_State* L);
	static int _setEndPosition(lua_State* L);
	static int _pathfindStep(lua_State* L);
	static int _addCharacter(lua_State* L);
};

#endif
