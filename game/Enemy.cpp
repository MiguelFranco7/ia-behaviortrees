#include "stdafx.h"
#include "Enemy.h"
#include "Player.h"
#include <tinyxml.h>
#include "stateMachine\StateMachine.h"
#include "stateMachine\State.h"
#include "stateMachine\AlarmAction.h"
#include "stateMachine\AttackAction.h"
#include "stateMachine\IdleAction.h"
#include "stateMachine\MoveEnemyAction.h"
#include "stateMachine\CanAttackCondition.h"
#include "stateMachine\CanSeePlayerCondition.h"
#include "stateMachine\ContinueAttackCondition.h"
#include "stateMachine\PlayerIsNearCondition.h"
#include "stateMachine\EnemyKillPlayerCondition.h"
#include "stateMachine\Transition.h"
#include "BehaviorTree\Selector.h"
#include "BehaviorTree\Sequence.h"
#include "BehaviorTree\Behaviors\Actions\AttackBehavior.h"
#include "BehaviorTree\Behaviors\Actions\IdleBehavior.h"
#include "BehaviorTree\Behaviors\Actions\MoveEnemyBehavior.h"
#include "BehaviorTree\Behaviors\Actions\AlarmBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\CanAttackConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\CanSeePlayerConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\ContinueAttackConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\EnemyKillPlayerConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\PlayerIsNearConditionBehavior.h"
#include <params.h>

Character* Enemy::mPlayerTarget;

Enemy::Enemy() {
	RTTI_BEGIN
		RTTI_EXTEND(MOAIEntity2D)
	RTTI_END

	mLinearVelocity  = { 0.0f, 0.0f };
	mAngularVelocity = 0.0f;
}

Enemy::~Enemy() {
	// TODO: Complete
}

void Enemy::OnStart() {
	ReadParams("sample/enemy_params.xml", mParams);

	// State Machine without file
	//InitStateMachine();
	
	// State Machine with file
	/*mStateMachine = new StateMachine();
	mStateMachine->load("sample/enemyStateMachine.xml", this);
	if (mStateMachine)
		mStateMachine->start();*/

	// Behavior without file
	//InitBehaviorTree();

	// Behavior with file
	mBehaviorTree = new Selector();
	mBehaviorTree->load("sample/enemyBehaviorTree.xml", this);
}

void Enemy::OnStop() {

}

void Enemy::OnUpdate(float step) {
	mDeltaTime = step;

	// Update State Machine
	//mStateMachine->update();

	// Update BehaviorTree
	if (mBehaviorTree)
		mBehaviorTree->tick();
}

void Enemy::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(0.0f, 0.0f, 1.0f, 0.5f);

	MOAIDraw::DrawPoint(0.0f, 0.0f);
}

bool Enemy::GetIsAttacking() {
	return mIsAttacking;
}

void Enemy::SetIsAttacking(bool isAttacking) {
	mIsAttacking = isAttacking;
}

void Enemy::MoveToPlayer() {
	USVec2D LocThis = GetLoc();
	USVec2D LocTarget = mPlayerTarget->GetLoc();
	USVec2D distancePlayer = mPlayerTarget->GetLoc() - GetLoc();

	if (distancePlayer.Length() < 10) {
		SetLoc({ 0.0f, 0.0f, 0.0f });
	}

	// PURSUE
	pursue = PursueSteering(this, mPlayerTarget);
	USVec2D acce = pursue.GetSteering();

	mLinearVelocity.mX += acce.mX * mDeltaTime;
	mLinearVelocity.mY += acce.mY * mDeltaTime;
	SetLoc(GetLoc() += mLinearVelocity * mDeltaTime);

	// ALIGN TO MOVEMENT
	alignToMovement = AlignToMovementSteering(this);
	float acceRot = alignToMovement.GetSteering();

	mAngularVelocity += acceRot * mDeltaTime;
	SetRot(GetRot() + mAngularVelocity * mDeltaTime);
}

void Enemy::InitStateMachine() {
	mStateMachine = new StateMachine();

	// CREATE STATES
	// - Idle
	State * idleState = new State();
	IdleAction * idleAction = new IdleAction(this, 0);
	idleState->setStateAction(idleAction);
	mStateMachine->addState(idleState);

	// - Alarm
	State * alarmState = new State();
	AlarmAction * alarmAction = new AlarmAction(this, 1);
	alarmState->setStateAction(alarmAction);
	mStateMachine->addState(alarmState);

	// - Attack
	State * attackState = new State();
	AttackAction * attackAction = new AttackAction(this, 2, 1.5f, 10.f);
	attackState->setStateAction(attackAction);
	mStateMachine->addState(attackState);

	// - Move
	State * moveState = new State();
	MoveEnemyAction * moveAction = new MoveEnemyAction(this, 5);
	moveState->setStateAction(moveAction);
	mStateMachine->addState(moveState);

	// CREATE TRANSITIONS
	// - Idle
	PlayerIsNearCondition * playerIsNearCondition = new PlayerIsNearCondition(this, 200.f);
	Transition * idleTransition = new Transition(playerIsNearCondition, alarmState, alarmAction);
	idleState->addTransitions(idleTransition);

	// - Alarm
	CanSeePlayerCondition * canSeePlayerCondition = new CanSeePlayerCondition(this, 150.f);
	Transition * alarmTransition = new Transition(canSeePlayerCondition, moveState, moveAction);
	alarmState->addTransitions(alarmTransition);

	// - Attack
	ContinueAttackCondition * continueAttackCondition = new ContinueAttackCondition(this);
	Transition * attackTransition = new Transition(continueAttackCondition, moveState, moveAction);
	attackState->addTransitions(attackTransition);

	// - Move
	EnemyKillPlayerCondition * enemyKillPlayerCondition = new EnemyKillPlayerCondition(this);
	Transition * attackKillTransition = new Transition(enemyKillPlayerCondition, idleState, idleAction);
	moveState->addTransitions(attackKillTransition);

	CanAttackCondition * canAttackCondition = new CanAttackCondition(this, 50.f);
	Transition * moveTransition = new Transition(canAttackCondition, attackState, attackAction);
	moveState->addTransitions(moveTransition);
}

void Enemy::InitBehaviorTree() {
	mBehaviorTree = new Selector();

	// Attack
	Sequence * attackSequence = new Sequence();
	AttackBehavior * attackBehavior = new AttackBehavior(this, 2, 1.2f, 10.f);
	CanAttackConditionBehavior * canAttackConditionBehavior = new CanAttackConditionBehavior(this, 50.f);
	EnemyKillPlayerConditionnBehavior * enemyKillPlayerConditionnBehavior = new EnemyKillPlayerConditionnBehavior(this);
	attackSequence->AddBehavior(enemyKillPlayerConditionnBehavior);
	attackSequence->AddBehavior(canAttackConditionBehavior);
	attackSequence->AddBehavior(attackBehavior);

	// Move
	Sequence * moveSequence = new Sequence();
	MoveEnemyBehavior * moveEnemyBehavior = new MoveEnemyBehavior(this, 5);
	CanSeePlayerConditionBehavior * canSeePlayerConditionBehavior = new CanSeePlayerConditionBehavior(this, 170.f);
	moveSequence->AddBehavior(enemyKillPlayerConditionnBehavior);
	moveSequence->AddBehavior(canSeePlayerConditionBehavior);
	moveSequence->AddBehavior(moveEnemyBehavior);

	// Alarm
	Sequence * alarmSequence = new Sequence();
	AlarmBehavior * alarmBehavior = new AlarmBehavior(this, 1);
	PlayerIsNearConditionBehavior * playerIsNearConditionBehavior = new PlayerIsNearConditionBehavior(this, 200.f);
	alarmSequence->AddBehavior(enemyKillPlayerConditionnBehavior);
	alarmSequence->AddBehavior(playerIsNearConditionBehavior);
	alarmSequence->AddBehavior(alarmBehavior);

	// Idle
	Sequence * idleSequence = new Sequence();
	IdleBehavior * idleBehavior = new IdleBehavior(this, 0);
	idleSequence->AddBehavior(idleBehavior);

	mBehaviorTree->AddBehavior(attackSequence);
	mBehaviorTree->AddBehavior(moveSequence);
	mBehaviorTree->AddBehavior(alarmSequence);
	mBehaviorTree->AddBehavior(idleSequence);
}


// Lua configuration

void Enemy::RegisterLuaFuncs(MOAILuaState & state) {
	MOAIEntity2D::RegisterLuaFuncs(state);

	luaL_Reg regTable[] = {
		{ "setLinearVel",			_setLinearVel },
		{ "setAngularVel",			_setAngularVel },
		{ "setTarget",				_setTarget },
		{ NULL, NULL }
	};

	luaL_register(state, 0, regTable);
}

int Enemy::_setLinearVel(lua_State* L) {
	MOAI_LUA_SETUP(Enemy, "U")

		float pX = state.GetValue<float>(2, 0.0f);
	float pY = state.GetValue<float>(3, 0.0f);
	self->SetLinearVelocity(pX, pY);
	return 0;
}

int Enemy::_setAngularVel(lua_State* L) {
	MOAI_LUA_SETUP(Enemy, "U")

		float angle = state.GetValue<float>(2, 0.0f);
	self->SetAngularVelocity(angle);

	return 0;
}

int Enemy::_setTarget(lua_State * L) {
	MOAI_LUA_SETUP(Enemy, "U")

	Player * player = state.GetLuaObject<Player>(2, true);

	if (player)
		mPlayerTarget = player;

	return 0;
}
