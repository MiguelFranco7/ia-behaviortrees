#pragma once

#include "character.h"

class Player : public Character {
public:
	DECL_LUA_FACTORY(Player)

	 Player();
	~Player();

	virtual void DrawDebug();
	virtual void UpdatePath(vector<USVec2D> pathfollowing);
	virtual void OnDamage(float damage);

	bool GetIsHitting();
	void SetIsHitting(bool isHitting);

protected:
	virtual void OnStart();
	virtual void OnStop();
	virtual void OnUpdate(float step);

private:
	USVec2D mRandomTarget;
	bool    mIsHitting;

	void MoveFollowingPath();

	virtual void InitStateMachine();
	virtual void InitBehaviorTree();


	// Lua configuration
public:
	virtual void RegisterLuaFuncs(MOAILuaState& state);
private:
	static int _setLinearVel(lua_State* L);
	static int _setAngularVel(lua_State* L);
};
