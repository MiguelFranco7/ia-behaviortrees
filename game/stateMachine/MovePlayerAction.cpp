#include "stdafx.h"
#include "MovePlayerAction.h"

MovePlayerAction::MovePlayerAction(Character * owner, int image) {
	mOwner = owner;
	mImage = image;
}

void MovePlayerAction::start() {
	Action::start();
}
