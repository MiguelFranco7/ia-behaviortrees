#pragma once
#include "Action.h"

class character;

class DeadAction : public Action {
public:
	DeadAction(Character * owner, int image);
	void start();
	void update() {}
	void end() {}
};
