#pragma once

class Character;

class Action {
public:
	virtual void start();
	virtual void update() {};
	virtual void end()	  {};

protected:
	Character * mOwner;
	int		    mImage;
};
