#pragma once
#include "Condition.h"

class Character;

class IsAliveCondition : public Condition {
public:
	IsAliveCondition(Character * owner);
	bool check() const;

private:
	Character * mOwner;
};
