#include "stdafx.h"
#include "State.h"
#include "Action.h"

State::~State() {
	if (mEnterAction)
		delete mEnterAction;

	if (mStateAction)
		delete mStateAction;

	if (mExitAction)
		delete mExitAction;
}

void State::onEnter() {
	if (mEnterAction) {
		mEnterAction->start();
		mEnterAction->update();
		mEnterAction->end();
	}

	if (mStateAction)
		mStateAction->start();
}

void State::update() {
	if (mStateAction)
		mStateAction->update();
}

void State::onExit() {
	if (mExitAction) {
		mExitAction->start();
		mExitAction->update();
		mExitAction->end();
	}

	if (mStateAction)
		mStateAction->end();
}

const Transitions & State::getTransitions() {
	return mTransitions;
}

void State::addTransitions(Transition * transition) {
	mTransitions.push_back(transition);
}

void State::setEnterAction(Action * enter) {
	mEnterAction = enter;
}

void State::setExitAction(Action * exit) {
	mExitAction = exit;
}

void State::setStateAction(Action * state) {
	mStateAction = state;
}
