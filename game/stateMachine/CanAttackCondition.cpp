#include "stdafx.h"
#include "CanAttackCondition.h"
#include "Enemy.h"

CanAttackCondition::CanAttackCondition(Enemy * owner, float distanceToAttack) {
	mOwner			  = owner;
	mDistanceToAttack = distanceToAttack;
}

bool CanAttackCondition::check() const {
	if (!mOwner->GetPlayerTarget())
		return false;

	if (mOwner->GetPlayerTarget()->GetCurrentHealth() <= 0.f)
		return false;

	USVec3D distanceVec = mOwner->GetLoc() - mOwner->GetPlayerTarget()->GetLoc();
	float distance      = distanceVec.Length();

	return distance < mDistanceToAttack;
}
