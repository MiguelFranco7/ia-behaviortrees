#include "stdafx.h"
#include "AndCondition.h"

AndCondition::AndCondition(Condition * c1, Condition * c2) {
	mC1 = c1;
	mC2 = c2;
}

bool AndCondition::check() const {
	if (mC1 && mC2)
		return mC1->check() && mC2->check();

	return false;
}
