#pragma once
#include "Condition.h"

class Enemy;

class CanAttackCondition : public Condition {
public:
	CanAttackCondition(Enemy * owner, float distanceToAttack);
	bool check() const;

private:
	Enemy * mOwner;
	float   mDistanceToAttack;
};
