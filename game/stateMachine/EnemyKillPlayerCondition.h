#pragma once
#include "Condition.h"

class Enemy;

class EnemyKillPlayerCondition : public Condition {
public:
	EnemyKillPlayerCondition(Enemy * owner);
	bool check() const;

private:
	Enemy * mOwner;
};
