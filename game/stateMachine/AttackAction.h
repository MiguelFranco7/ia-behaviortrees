#pragma once
#include "Action.h"

class Enemy;

class AttackAction : public Action {
public:
	AttackAction(Enemy * owner, int image, float attackDuration, float damageAttack);
	void start();
	void update();
	void end() {}

private:
	float mAttackDuration;
	float mCurrentAttackDuration;
	float mDamageAttack;
};
