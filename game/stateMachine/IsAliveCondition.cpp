#include "stdafx.h"
#include "IsAliveCondition.h"
#include "Character.h"

IsAliveCondition::IsAliveCondition(Character * owner) {
	mOwner = owner;
}

bool IsAliveCondition::check() const {
	return mOwner->GetCurrentHealth() <= 0.f;
}
