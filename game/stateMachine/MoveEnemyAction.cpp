#include "stdafx.h"
#include "MoveEnemyAction.h"
#include "Enemy.h"

MoveEnemyAction::MoveEnemyAction(Character * owner, int image) {
	mOwner = owner;
	mImage = image;
}

void MoveEnemyAction::start() {
	Action::start();
}

void MoveEnemyAction::update() {
	Enemy * enemy = static_cast<Enemy *>(mOwner);

	if (enemy->GetPlayerTarget() && enemy->GetPlayerTarget()->GetCurrentHealth() > 0.f)
		enemy->MoveToPlayer();
}
