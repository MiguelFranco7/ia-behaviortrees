#pragma once

#include <iostream>
#include <vector>

class State;
class Character;

class StateMachine {
public:
	~StateMachine();

	bool load(const char * filename, Character * owner);
	void start();
	void update();
	void end();
	void addState(State * state);

private:
	std::vector<State*> mStates;
	State * mCurrentState;
};
