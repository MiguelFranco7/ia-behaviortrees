#include "stdafx.h"
#include "EnemyKillPlayerCondition.h"
#include "Enemy.h"

EnemyKillPlayerCondition::EnemyKillPlayerCondition(Enemy * owner) {
	mOwner = owner;
}

bool EnemyKillPlayerCondition::check() const {
	if (!mOwner->GetPlayerTarget())
		return true;

	if (mOwner->GetPlayerTarget()->GetCurrentHealth() > 0.f)
		return false;
	else
		return true;
}
