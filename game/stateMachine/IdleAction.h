#pragma once
#include "Action.h"

class Enemy;

class IdleAction : public Action {
public:
	IdleAction(Enemy * owner, int image);
	virtual void start();
	virtual void update() {}
	virtual void end()    {}
};
