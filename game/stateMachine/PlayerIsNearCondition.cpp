#include "stdafx.h"
#include "PlayerIsNearCondition.h"
#include "Enemy.h"

PlayerIsNearCondition::PlayerIsNearCondition(Enemy * owner, float distanceToAlert) {
	mOwner			 = owner;
	mDistanceToAlert = distanceToAlert;
}

bool PlayerIsNearCondition::check() const {
	if (!mOwner->GetPlayerTarget())
		return false;

	if (mOwner->GetPlayerTarget()->GetCurrentHealth() <= 0.f)
		return false;

	USVec3D distanceVec = mOwner->GetLoc() - mOwner->GetPlayerTarget()->GetLoc();
	float distance		= distanceVec.Length();

	return distance < mDistanceToAlert;
}
