#include "stdafx.h"
#include "ContinueAttackCondition.h"
#include "Enemy.h"

ContinueAttackCondition::ContinueAttackCondition(Enemy * owner) {
	mOwner = owner;
}

bool ContinueAttackCondition::check() const {
	return !mOwner->GetIsAttacking();
}
