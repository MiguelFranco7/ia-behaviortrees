#include "stdafx.h"
#include "ContinueHitCondition.h"
#include "Player.h"

ContinueHitCondition::ContinueHitCondition(Player * owner) {
	mOwner = owner;
}

bool ContinueHitCondition::check() const {
	return !mOwner->GetIsHitting();
}
