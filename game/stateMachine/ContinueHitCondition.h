#pragma once
#include "Condition.h"

class Player;

class ContinueHitCondition : public Condition {
public:
	ContinueHitCondition(Player * owner);
	bool check() const;

private:
	Player * mOwner;
};
