#pragma once
#include "Action.h"
#include "uslscore\USVec2D.h"

class GoToAction : public Action {
public:
	void start() { /* pathfinding to destination */ }
	void update() { /* move throught path */ }
	GoToAction(const USVec3D & pos);

private:
	USVec3D mDestination;
};
