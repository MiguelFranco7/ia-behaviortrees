#include "stdafx.h"
#include "Transition.h"
#include "Condition.h"
#include "Action.h"

Transition::Transition(Condition * condition, State * state, Action * action) {
	mCondition	   = condition;
	mTargetState   = state;
	mTriggerAction = action;
}

Transition::~Transition() {
	if (mCondition)
		delete mCondition;

	if (mTargetState)
		delete mTargetState;

	if (mTriggerAction)
		delete mTriggerAction;
}

bool Transition::canTrigger() const {
	if (!mCondition || !mTargetState)
		return false;
	else
		return mCondition->check();
}

State * Transition::trigger() const {
	/*if (mTriggerAction) {
		mTriggerAction->start();
		mTriggerAction->update();
		mTriggerAction->end();
	}*/

	return mTargetState;
}
