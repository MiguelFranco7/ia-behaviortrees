#include "stdafx.h"
#include <tinyxml.h>
#include "StateMachine.h"
#include "State.h"
#include "Transition.h"
#include "character.h"
#include "AlarmAction.h"
#include "AttackAction.h"
#include "IdleAction.h"
#include "MoveEnemyAction.h"
#include "CanAttackCondition.h"
#include "CanSeePlayerCondition.h"
#include "ContinueAttackCondition.h"
#include "PlayerIsNearCondition.h"
#include "EnemyKillPlayerCondition.h"
#include "MovePlayerAction.h"
#include "HitAction.h"
#include "DeadAction.h"
#include "ContinueHitCondition.h"
#include "IsAliveCondition.h"
#include "ReceiveHitCondition.h"
#include "Enemy.h"
#include "Player.h"

struct cmp_str {
	bool operator()(const char const * a, const char const * b) const {
		return std::strcmp(a, b) < 0;
	}
};

StateMachine::~StateMachine() {
	for (int i = mStates.size() - 1; i >= 0; --i) {
		delete mStates[i];
	}

	if (mCurrentState)
		delete mCurrentState;
}

bool StateMachine::load(const char * filename, Character * owner) {
	TiXmlDocument doc(filename);
	if (!doc.LoadFile())
	{
		fprintf(stderr, "Couldn't read params from %s", filename);
		return false;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement* pElem;
	pElem = hDoc.FirstChildElement().Element();
	if (!pElem)
	{
		fprintf(stderr, "Invalid format for %s", filename);
		return false;
	}

	TiXmlHandle hRoot(pElem);
	map<const char *, State *, cmp_str> statesToAdd;

	// STATES
	TiXmlHandle hStates = hRoot.FirstChildElement("States");
	TiXmlElement * stateElem = hStates.FirstChild("State").Element();
	for (stateElem; stateElem; stateElem = stateElem->NextSiblingElement()) {
		const char * stateFunction = stateElem->Attribute("function");

		State * state = new State();
		addState(state);
		statesToAdd[stateFunction] = state;

		TiXmlElement * actionElem = stateElem->FirstChildElement("Action");
		for (actionElem; actionElem; actionElem = actionElem->NextSiblingElement()) {
			const char * actionFunction = actionElem->Attribute("function");
			const char * actionType = actionElem->Attribute("type");
			int indexImage = 0;
			actionElem->Attribute("indexImage", &indexImage);

			if (!strcmp(actionFunction, "Idle")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy) {
					IdleAction * idleAction = new IdleAction(enemy, indexImage);

					if (!strcmp(actionType, "Enter"))
						state->setEnterAction(idleAction);
					else if (!strcmp(actionType, "State"))
						state->setStateAction(idleAction);
					else if (!strcmp(actionType, "Exit"))
						state->setExitAction(idleAction);
				}
			}
			else if (!strcmp(actionFunction, "Alarm")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy) {
					AlarmAction * alarmAction = new AlarmAction(enemy, indexImage);

					if (!strcmp(actionType, "Enter"))
						state->setEnterAction(alarmAction);
					else if (!strcmp(actionType, "State"))
						state->setStateAction(alarmAction);
					else if (!strcmp(actionType, "Exit"))
						state->setExitAction(alarmAction);
				}
			}
			else if (!strcmp(actionFunction, "Attack")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy) {
					float attackDuration;
					float damageAttack;
					actionElem->Attribute("attackDuration", &attackDuration);
					actionElem->Attribute("damage", &damageAttack);

					AttackAction * attackAction = new AttackAction(enemy, indexImage, attackDuration, damageAttack);

					if (!strcmp(actionType, "Enter"))
						state->setEnterAction(attackAction);
					else if (!strcmp(actionType, "State"))
						state->setStateAction(attackAction);
					else if (!strcmp(actionType, "Exit"))
						state->setExitAction(attackAction);
				}
			}
			else if (!strcmp(actionFunction, "MoveEnemy")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy) {
					MoveEnemyAction * moveAction = new MoveEnemyAction(enemy, indexImage);

					if (!strcmp(actionType, "Enter"))
						state->setEnterAction(moveAction);
					else if (!strcmp(actionType, "State"))
						state->setStateAction(moveAction);
					else if (!strcmp(actionType, "Exit"))
						state->setExitAction(moveAction);
				}
			}
			else if (!strcmp(actionFunction, "MovePlayer")) {
				MovePlayerAction * moveAction = new MovePlayerAction(owner, indexImage);

				if (!strcmp(actionType, "Enter"))
					state->setEnterAction(moveAction);
				else if (!strcmp(actionType, "State"))
					state->setStateAction(moveAction);
				else if (!strcmp(actionType, "Exit"))
					state->setExitAction(moveAction);
			}
			else if (!strcmp(actionFunction, "Dead")) {
				DeadAction * deadAction = new DeadAction(owner, indexImage);

				if (!strcmp(actionType, "Enter"))
					state->setEnterAction(deadAction);
				else if (!strcmp(actionType, "State"))
					state->setStateAction(deadAction);
				else if (!strcmp(actionType, "Exit"))
					state->setExitAction(deadAction);
			}
			else if (!strcmp(actionFunction, "Hit")) {
				float hitDuration = 0.f;
				actionElem->Attribute("hitDuration", &hitDuration);
				//actionElem->QueryFloatAttribute("attackDuration", &hitDuration);

				HitAction * hitAction = new HitAction(owner, indexImage, hitDuration);

				if (!strcmp(actionType, "Enter"))
					state->setEnterAction(hitAction);
				else if (!strcmp(actionType, "State"))
					state->setStateAction(hitAction);
				else if (!strcmp(actionType, "Exit"))
					state->setExitAction(hitAction);
			}
		}
	}

	// TRANSITIONS
	TiXmlHandle hTransitions = hRoot.FirstChildElement("Transitions");
	TiXmlElement * transitionElem = hTransitions.FirstChild("Transition").Element();
	for (transitionElem; transitionElem; transitionElem = transitionElem->NextSiblingElement()) {

		TiXmlElement * conditionElem = transitionElem->FirstChildElement("Condition");
		for (conditionElem; conditionElem; conditionElem = conditionElem->NextSiblingElement()) {
			const char * conditionFunction    = conditionElem->Attribute("function");
			const char * conditionActualState = conditionElem->Attribute("actualState");
			const char * conditionNextState   = conditionElem->Attribute("nextState");

			Condition * condition = nullptr;
			if (!strcmp(conditionFunction, "PlayerIsNear")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy) {
					float distance;
					conditionElem->Attribute("distance", &distance);
					condition = new PlayerIsNearCondition(enemy, distance);
				}
			}
			else if (!strcmp(conditionFunction, "CanSeePlayer")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy) {
					float distance;
					conditionElem->Attribute("distance", &distance);
					condition = new CanSeePlayerCondition(enemy, distance);
				}
			}
			else if (!strcmp(conditionFunction, "ContinueAttack")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy)
					condition = new ContinueAttackCondition(enemy);
			}
			else if (!strcmp(conditionFunction, "EnemyKillPlayer")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy)
					condition = new EnemyKillPlayerCondition(enemy);
			}
			else if (!strcmp(conditionFunction, "CanAttack")) {
				Enemy * enemy = static_cast<Enemy *>(owner);

				if (enemy) {
					float distanceToAttack;
					conditionElem->Attribute("distance", &distanceToAttack);
					condition = new CanAttackCondition(enemy, distanceToAttack);
				}
			}
			else if (!strcmp(conditionFunction, "ReceiveHit")) {
				Player * player = static_cast<Player *>(owner);

				if (player) {
					condition = new ReceiveHitCondition(player);
				}
			}
			else if (!strcmp(conditionFunction, "ContinueHit")) {
				Player * player = static_cast<Player *>(owner);

				if (player) {
					condition = new ContinueHitCondition(player);
				}
			}
			else if (!strcmp(conditionFunction, "IsAlive")) {
				condition = new IsAliveCondition(owner);
			}

			Transition * transition = new Transition(condition, statesToAdd.at(conditionNextState), nullptr);
			statesToAdd.at(conditionActualState)->addTransitions(transition);
		}
	}

	return true;
}

void StateMachine::start() {
	mCurrentState->onEnter();
}

void StateMachine::update() {
	mCurrentState->update();
	const Transitions & trans = mCurrentState->getTransitions();
	for (int i = 0; i < trans.size(); ++i) {
		if (trans[i]->canTrigger()) {
			mCurrentState->onExit();
			State * nextState = trans[i]->trigger();
			nextState->onEnter();
			mCurrentState = nextState;
			return;
		}
	}
}

void StateMachine::end() {
	mCurrentState->onExit();
}

void StateMachine::addState(State * state) {
	mStates.push_back(state);

	if (mStates.size() == 1)
		mCurrentState = state;
}
