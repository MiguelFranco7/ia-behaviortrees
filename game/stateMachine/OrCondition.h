#pragma once

#include "Condition.h"

class OrCondition : public Condition {
public:
	OrCondition(Condition * c1, Condition * c2);
	bool check() const;

private:
	Condition * mC1;
	Condition * mC2;
};
