#pragma once
#include "Condition.h"

class Enemy;

class CanSeePlayerCondition : public Condition {
public:
	CanSeePlayerCondition(Enemy * owner, float distanceSeePlayer);
	bool check() const;

private:
	Enemy * mOwner;
	float mDistanceSeePlayer;
};
