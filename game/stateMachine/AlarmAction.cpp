#include "stdafx.h"
#include "AlarmAction.h"
#include "Enemy.h"

AlarmAction::AlarmAction(Enemy * owner, int image) {
	mOwner = owner;
	mImage = image;
}

void AlarmAction::start() {
	Action::start();
}
