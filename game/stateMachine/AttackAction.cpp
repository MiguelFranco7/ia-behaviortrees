#include "stdafx.h"
#include "AttackAction.h"
#include "Enemy.h"

AttackAction::AttackAction(Enemy * owner, int image, float attackDuration, float damageAttack) {
	mOwner			= owner;
	mImage			= image;
	mAttackDuration = attackDuration;
	mDamageAttack   = damageAttack;
}

void AttackAction::start() {
	Action::start();

	mCurrentAttackDuration = 0.f;
	Enemy * enemy = static_cast<Enemy *>(mOwner);
	enemy->SetIsAttacking(true);
	Character * player = enemy->GetPlayerTarget();
	player->OnDamage(mDamageAttack);
}

void AttackAction::update() {
	mCurrentAttackDuration += mOwner->GetCurrentDeltaTime();

	if (mCurrentAttackDuration >= mAttackDuration) {
		Enemy * enemy = static_cast<Enemy *>(mOwner);
		enemy->SetIsAttacking(false);
	}
}
