#pragma once
#include "Action.h"

class Enemy;

class AlarmAction : public Action {
public:
	AlarmAction(Enemy * owner, int image);
	void start();
	void update() {}
	void end() {}
};
