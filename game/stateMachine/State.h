#pragma once

class Action;
class Transition;

typedef vector<Transition *> Transitions;

class State {
public:
	~State();

	void onEnter();
	void update();
	void onExit();
	const Transitions & getTransitions();
	void addTransitions(Transition *transition);

	void setEnterAction(Action * enter);
	void setExitAction(Action * exit);
	void setStateAction(Action *state);

private:
	Action	  * mEnterAction;
	Action	  * mExitAction;
	Action	  * mStateAction;
	Transitions mTransitions;
};
