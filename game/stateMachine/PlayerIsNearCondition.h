#pragma once
#include "Condition.h"

class Enemy;

class PlayerIsNearCondition : public Condition {
public:
	PlayerIsNearCondition(Enemy * owner, float distanceToAlert);
	virtual bool check() const;

private:
	Enemy * mOwner;
	float	mDistanceToAlert;
};