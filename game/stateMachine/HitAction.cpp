#include "stdafx.h"
#include "HitAction.h"
#include "Player.h"

HitAction::HitAction(Character * owner, int image, float hitDuration) {
	mOwner		 = owner;
	mImage		 = image;
	mHitDuration = hitDuration;
}

void HitAction::start() {
	Action::start();

	mCurrentHitDuration = 0.f;
	Player * player = static_cast<Player *>(mOwner);
	player->SetIsHitting(true);
}

void HitAction::update() {
	mCurrentHitDuration += mOwner->GetCurrentDeltaTime();

	if (mCurrentHitDuration >= mHitDuration) {
		Player * player = static_cast<Player *>(mOwner);
		player->SetIsHitting(false);
	}
}
