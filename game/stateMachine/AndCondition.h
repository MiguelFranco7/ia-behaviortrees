#pragma once
#include "Condition.h"

class AndCondition : public Condition {
public:
	AndCondition(Condition * c1, Condition * c2);
	bool check() const;

private:
	Condition * mC1;
	Condition * mC2;
};
