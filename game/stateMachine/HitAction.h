#pragma once
#include "Action.h"

class Character;

class HitAction : public Action {
public:
	HitAction(Character * owner, int image, float hitDuration);
	void start();
	void update();
	void end() {}

private:
	float mHitDuration;
	float mCurrentHitDuration;
};
