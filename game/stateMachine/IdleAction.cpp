#include "stdafx.h"
#include "IdleAction.h"
#include "Enemy.h"

IdleAction::IdleAction(Enemy * owner, int image) {
	mOwner = owner;
	mImage = image;
}

void IdleAction::start() {
	Action::start();
}
