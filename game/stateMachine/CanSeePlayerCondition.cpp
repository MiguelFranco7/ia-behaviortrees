#include "stdafx.h"
#include "CanSeePlayerCondition.h"
#include "Enemy.h"

CanSeePlayerCondition::CanSeePlayerCondition(Enemy * owner, float distanceSeePlayer) {
	mOwner			   = owner;
	mDistanceSeePlayer = distanceSeePlayer;
}

bool CanSeePlayerCondition::check() const {
	if (!mOwner->GetPlayerTarget())
		return false;

	USVec3D distanceVec = mOwner->GetLoc() - mOwner->GetPlayerTarget()->GetLoc();
	float distance      = distanceVec.Length();

	return distance < mDistanceSeePlayer;
}
