#pragma once
#include "Action.h"

class Character;

class MoveEnemyAction : public Action {
public:
	MoveEnemyAction(Character * owner, int image);
	void start();
	void update();
	void end() {}
};
