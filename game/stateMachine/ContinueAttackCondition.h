#pragma once
#include "Condition.h"

class Enemy;

class ContinueAttackCondition : public Condition {
public:
	ContinueAttackCondition(Enemy * owner);
	bool check() const;

private:
	Enemy * mOwner;
};
