#include "stdafx.h"
#include "DeadAction.h"

DeadAction::DeadAction(Character * owner, int image) {
	mOwner = owner;
	mImage = image;
}

void DeadAction::start() {
	Action::start();
}
