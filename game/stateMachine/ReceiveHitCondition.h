#pragma once
#include "Condition.h"

class Player;

class ReceiveHitCondition : public Condition {
public:
	ReceiveHitCondition(Player * owner);
	bool check() const;

private:
	Player * mOwner;
};
