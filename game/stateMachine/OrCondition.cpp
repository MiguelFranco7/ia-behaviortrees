#include "stdafx.h"
#include "OrCondition.h"

OrCondition::OrCondition(Condition * c1, Condition * c2) {
	mC1 = c1;
	mC2 = c2;
}

bool OrCondition::check() const {
	if (mC1 && mC2)
		return mC1->check() || mC2->check();

	return false;
}
