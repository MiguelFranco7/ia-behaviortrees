#include "stdafx.h"
#include "ReceiveHitCondition.h"
#include "Player.h"

ReceiveHitCondition::ReceiveHitCondition(Player * owner) {
	mOwner = owner;
}

bool ReceiveHitCondition::check() const {
	return mOwner->GetIsHitting();
}
