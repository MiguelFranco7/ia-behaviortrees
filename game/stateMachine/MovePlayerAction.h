#pragma once
#include "Action.h"

class Character;

class MovePlayerAction : public Action {
public:
	MovePlayerAction(Character * owner, int image);
	void start();
	void update() {}
	void end() {}
};
