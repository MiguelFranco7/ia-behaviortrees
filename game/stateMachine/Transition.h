#pragma once

class Condition;
class State;
class Action;

class Transition {
public:
	Transition(Condition * condition, State * state, Action * action);
	~Transition();

	bool	canTrigger() const;
	State * trigger()	 const;

private:
	Condition * mCondition;
	State	  * mTargetState;
	Action	  * mTriggerAction;
};
