#include "stdafx.h"
#include "Player.h"
#include "stateMachine\StateMachine.h"
#include "stateMachine\State.h"
#include "stateMachine\MovePlayerAction.h"
#include "stateMachine\HitAction.h"
#include "stateMachine\DeadAction.h"
#include "stateMachine\ContinueHitCondition.h"
#include "stateMachine\IsAliveCondition.h"
#include "stateMachine\ReceiveHitCondition.h"
#include "stateMachine\Transition.h"
#include "BehaviorTree\Selector.h"
#include "BehaviorTree\Sequence.h"
#include "BehaviorTree\Behaviors\Actions\HitBehavior.h"
#include "BehaviorTree\Behaviors\Actions\DeadBehavior.h"
#include "BehaviorTree\Behaviors\Actions\MovePlayerBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\ContinueHitConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\IsAliveConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\ReceiveHitConditionBehavior.h"
#include <tinyxml.h>
#include <params.h>

Player::Player() {
	RTTI_BEGIN
		RTTI_EXTEND(MOAIEntity2D)
	RTTI_END

	mLinearVelocity  = { 0.0f, 0.0f };
	mAngularVelocity = 0.0f;
	mIsHitting		 = false;
}

Player::~Player() {
	// TODO: Complete
}

void Player::OnStart() {
	ReadParams("sample/params.xml", mParams);

	pathFollowing = PathFollowingSteering(this, mPath);

	mCurrentHealth = mMaxHealth;

	mDeltaTime = 0.f;

	// State Machine without file
	//InitStateMachine();
	
	// State Machine with file
	/*mStateMachine = new StateMachine();
	mStateMachine->load("sample/playerStateMachine.xml", this);
	if (mStateMachine)
		mStateMachine->start();*/

	// Behavior without file
	//InitBehaviorTree();

	// Behavior with file
	mBehaviorTree = new Selector();
	mBehaviorTree->load("sample/playerBehaviorTree.xml", this);
}

void Player::OnStop() {

}

void Player::OnUpdate(float step) {
	// Update mDeltaTime
	mDeltaTime = step;

	// Update State Machine
	//mStateMachine->update();

	if (mBehaviorTree)
		mBehaviorTree->tick();

	if (mCurrentHealth > 0) {
		MoveFollowingPath();
	}
}

void Player::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(0.0f, 0.0f, 1.0f, 0.5f);

	pathFollowing.DrawDebug();

	// Pintado del Path del character.
	if (mPath.size() > 0) {
		gfxDevice.SetPenColor(0.0f, 0.3f, 1.0f, 0.5f);
		for (int i = 0; i < mPath.size() - 1; i++) {
			MOAIDraw::DrawLine(mPath[i], mPath[i + 1]);
		}
	}

	for (int i = 0; i < mPath.size(); i++) {
		MOAIDraw::DrawEllipseFill(mPath[i].mX, mPath[i].mY, 15, 15, 100);
	}
}

void Player::UpdatePath(vector<USVec2D> pathfollowing) {
	mPath = pathfollowing;
	pathFollowing = PathFollowingSteering(this, mPath);
}

void Player::OnDamage(float damage) {
	Character::OnDamage(damage);

	mIsHitting = true;
}

bool Player::GetIsHitting() {
	return mIsHitting;
}

void Player::SetIsHitting(bool isHitting) {
	mIsHitting = isHitting;
}

void Player::InitStateMachine() {
	mStateMachine = new StateMachine();

	// Create States
	// - Move
	State * moveState = new State();
	MovePlayerAction * moveAction = new MovePlayerAction(this, 5);
	moveState->setStateAction(moveAction);
	mStateMachine->addState(moveState);

	// - Dead
	State * deadState = new State();
	DeadAction * deadAction = new DeadAction(this, 3);
	deadState->setStateAction(deadAction);
	mStateMachine->addState(deadState);

	// - Hit
	State * hitState = new State();
	HitAction * hitAction = new HitAction(this, 4, 0.7f);
	hitState->setStateAction(hitAction);
	mStateMachine->addState(hitState);

	// Create transitions
	// - Move
	ReceiveHitCondition * receiveHitCondition = new ReceiveHitCondition(this);
	Transition * moveTransition = new Transition(receiveHitCondition, hitState, hitAction);
	moveState->addTransitions(moveTransition);

	// - Hit
	ContinueHitCondition * continueHitCondition = new ContinueHitCondition(this);
	Transition * hitTransition = new Transition(continueHitCondition, moveState, moveAction);
	hitState->addTransitions(hitTransition);

	// - Hit Dead
	IsAliveCondition * isAliveCondition = new IsAliveCondition(this);
	Transition * deadTransition = new Transition(isAliveCondition, deadState, deadAction);
	hitState->addTransitions(deadTransition);
}

void Player::InitBehaviorTree() {
	mBehaviorTree = new Selector();

	// Hit Dead
	Sequence * deadSequence = new Sequence();
	DeadBehavior * deadBehavior = new DeadBehavior(this, 3);
	IsAliveConditionBehavior * isAliveCondition = new IsAliveConditionBehavior(this);
	deadSequence->AddBehavior(isAliveCondition);
	deadSequence->AddBehavior(deadBehavior);

	// Hit
	Sequence * hitSequence = new Sequence();
	HitBehavior * hitBehavior = new HitBehavior(this, 4, 0.7f);
	ContinueHitConditionBehavior * continueHitCondition = new ContinueHitConditionBehavior(this);
	hitSequence->AddBehavior(continueHitCondition);
	hitSequence->AddBehavior(hitBehavior);

	// Move
	Sequence * movePlayerSequence = new Sequence();
	MovePlayerBehavior * movePlayerBehavior = new MovePlayerBehavior(this, 5);
	movePlayerSequence->AddBehavior(movePlayerBehavior);

	mBehaviorTree->AddBehavior(deadSequence);
	mBehaviorTree->AddBehavior(hitSequence);
	mBehaviorTree->AddBehavior(movePlayerSequence);
}


// Lua configuration

void Player::MoveFollowingPath() {
	// PATH FOLLOWING
	USVec2D acce = pathFollowing.GetSteering();

	// OBSTACLE AVOIDANCE
	/*obstacleAvoidance = ObstacleAvoidanceSteering(this);
	USVec2D acceObs = obstacleAvoidance.GetSteering();*/

	mLinearVelocity.mX += acce.mX * mDeltaTime;// + acceObs.mX;
	mLinearVelocity.mY += acce.mY * mDeltaTime;// + acceObs.mY;
	SetLoc(GetLoc() += mLinearVelocity * mDeltaTime);

	// ALIGN TO MOVEMENT
	alignToMovement = AlignToMovementSteering(this);
	float acceRot = alignToMovement.GetSteering();

	mAngularVelocity += acceRot * mDeltaTime;
	SetRot(GetRot() + mAngularVelocity * mDeltaTime);
}

void Player::RegisterLuaFuncs(MOAILuaState & state) {
	MOAIEntity2D::RegisterLuaFuncs(state);

	luaL_Reg regTable[] = {
		{ "setLinearVel",			_setLinearVel },
		{ "setAngularVel",			_setAngularVel },
		{ NULL, NULL }
	};

	luaL_register(state, 0, regTable);
}

int Player::_setLinearVel(lua_State* L) {
	MOAI_LUA_SETUP(Player, "U")

	float pX = state.GetValue<float>(2, 0.0f);
	float pY = state.GetValue<float>(3, 0.0f);
	self->SetLinearVelocity(pX, pY);
	return 0;
}

int Player::_setAngularVel(lua_State* L) {
	MOAI_LUA_SETUP(Player, "U")

	float angle = state.GetValue<float>(2, 0.0f);
	self->SetAngularVelocity(angle);

	return 0;
}
