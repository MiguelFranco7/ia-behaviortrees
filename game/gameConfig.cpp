#include <stdafx.h>
#include "gameConfig.h"

#include "pathfinding/pathfinder.h"
#include "Enemy.h"
#include "Player.h"

void Configure(MOAIGlobals* globals)
{
	REGISTER_LUA_CLASS(Character)
	REGISTER_LUA_CLASS(Pathfinder)
	REGISTER_LUA_CLASS(Enemy)
	REGISTER_LUA_CLASS(Player)
}
