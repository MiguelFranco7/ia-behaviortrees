#ifndef __PARAMS_H__
#define __PARAMS_H__

struct Params {
	USVec2D targetPosition;
	float max_velocity;
	float max_acceleration;
	float dest_radius;
	float arrive_radius;

	float max_angular_velocity;
	float max_angular_acceleration;
	float angular_dest_radius;
	float angular_arrive_radius;
	float targetRotation;

	float charRadius;

	float lookAhead;
	float timeAhead;
};

struct NavPolygon {
	struct Edge {
		int mVerts[2]; // Indices
		NavPolygon* mPNeighbour;
	};
	//TerrainType mTerrain;
	int _id;
	vector<USVec2D> mVerts;
	vector<Edge>	mEdges;

	USVec2D		pointPosition;
	float		actualCost;
	float		totalCost;
	int			cost;
	float		costToEnd;
	NavPolygon*	father;
	bool isOpen;
	bool isClose;
};

struct Obstacles {
	USVec2D position;
	float	radius;
	bool	collision;
};

bool ReadParams(const char* filename, Params& params);
bool ReadPath(const char* filename, vector<USVec2D> &path);
bool ReadNav(const char* filename, vector<NavPolygon> &navMesh);
bool ReadObstacles(const char* filename, vector<Obstacles> &obstacles);

#endif