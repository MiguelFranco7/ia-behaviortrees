#include "stdafx.h"
#include "ObstacleAvoidanceSteering.h"
#include "character.h"

ObstacleAvoidanceSteering::ObstacleAvoidanceSteering(Character * character) {
	mCharacter = character;
}

USVec2D ObstacleAvoidanceSteering::GetSteering() {
	mAcce = USVec2D(0, 0);

	USVec2D np = mCharacter->GetLinearVelocity();
	np.NormSafe(); // np = normalizar(v)

	USVec2D lp(np * mCharacter->GetParams().lookAhead); // lp = np * lookahead

	for (int i = 0; i < mCharacter->GetObstacles().size(); i++) {
		USVec2D obsPos(mCharacter->GetObstacles()[i].position.mX, mCharacter->GetObstacles()[i].position.mY);
		USVec2D ro = obsPos - mCharacter->GetLoc(); // ro = po - pp
		float proj = ro.Dot(np); // proj = ro * np			

		USVec2D cercano;
		bool canCollision = true;
		if (proj > lp.Length()) // if (proj > len * lp) 
			cercano = mCharacter->GetLoc() + lp; // cercano = pp + lp
		else if (proj > 0)
			cercano = mCharacter->GetLoc() + np * proj; // cercano = pp + np * proj
		else
			canCollision = false;

		if (canCollision) {
			USVec2D diff = cercano - obsPos; // diff = cercano - po
			float dist = diff.Length();
			if (dist < mCharacter->GetObstacles()[i].radius + mCharacter->GetParams().charRadius) { // if (dist < radio)
				// collision
				mCharacter->SetObstacleCollision(i, true);
				float dot = ro.Cross(lp);
				if (dot > 0)
					ro.Rotate90Clockwise();
				else
					ro.Rotate90Anticlockwise();

				mAcce = ro;
			} else {
				// no collision
				mCharacter->SetObstacleCollision(i, false);
			}
		} else
			mCharacter->SetObstacleCollision(i, false);
	}

	return mAcce;
}

void ObstacleAvoidanceSteering::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();

	gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);

	// Vector (L�nea) de la velocidad deseada calculada en el frame anterior
	MOAIDraw::DrawLine(USVec2D(mCharacter->GetLoc().mX, mCharacter->GetLoc().mY), USVec2D(mCharacter->GetLoc().mX + mCharacter->GetLinearVelocity().mX, mCharacter->GetLoc().mY + mCharacter->GetLinearVelocity().mY));

	gfxDevice.SetPenColor(0.0f, 1.0f, 0.0f, 0.5f);
	// Vector (L�nea) de la aceleraci�n calculada en el frame anterior 
	MOAIDraw::DrawLine(USVec2D(mCharacter->GetLoc().mX, mCharacter->GetLoc().mY), USVec2D(mCharacter->GetLoc().mX + mAcce.mX, mCharacter->GetLoc().mY + mAcce.mY));
}
