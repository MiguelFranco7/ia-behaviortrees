function createImage(texture_name, char_size)
	local gfxQuad = MOAIGfxQuad2D.new()
	gfxQuad:setTexture(texture_name)
	char_size = 64
	gfxQuad:setRect(-char_size/2, -char_size/2, char_size/2, char_size/2)
	gfxQuad:setUVRect(0, 0, 1, 1)
	return gfxQuad
end

MOAISim.openWindow("game", 1024, 1000)

viewport = MOAIViewport.new()
viewport:setSize (1024, 1000)
viewport:setScale (1024, -1000)

layer = MOAILayer2D.new()
layer:setViewport(viewport)
MOAISim.pushRenderPass(layer)

char_size = 64
idle   = createImage("sample/dragon_sprites/dragon_idle.png",   char_size)
alarm  = createImage("sample/dragon_sprites/dragon_alarm.png",  char_size)
attack = createImage("sample/dragon_sprites/dragon_attack.png", char_size)
dead   = createImage("sample/dragon_sprites/dragon_dead.png",   char_size)
hit    = createImage("sample/dragon_sprites/dragon_hit.png",    char_size)
windup = createImage("sample/dragon_sprites/dragon_windup.png", char_size)

function drawBackground(image, sizeX, sizeY)
    gfxQuad = MOAIGfxQuad2D.new()
    gfxQuad:setTexture(image)
    gfxQuad:setRect(-sizeX/2, -sizeY/2, sizeX/2, sizeY/2)
    gfxQuad:setUVRect(0, 0, 1, 1)
    prop = MOAIProp2D.new()
    prop:setDeck(gfxQuad)
    prop:setLoc(posX, posY)
    layer:insertProp(prop)
end

drawBackground("sample/supermario.png", 1024, 1100)

-- Create Dragon
texture_name = "sample/dragon.png"
gfxQuad = MOAIGfxQuad2D.new()
gfxQuad:setTexture(texture_name)
char_size = 64
gfxQuad:setRect(-char_size/2, -char_size/2, char_size/2, char_size/2)
gfxQuad:setUVRect(0, 0, 1, 1)
  
prop = MOAIProp2D.new()
prop:setDeck(gfxQuad)

entity = Player.new()

-- Add prop to be the renderable for this player character
entity:addImage(idle)   -- SetImage(0)  dragon_idle.png
entity:addImage(alarm)  -- SetImage(1)  dragon_alarm.png
entity:addImage(attack) -- SetImage(2)  dragon_attack.png
entity:addImage(dead)   -- SetImage(3)  dragon_dead.png
entity:addImage(hit)    -- SetImage(4)  dragon_hit.png
entity:addImage(windup) -- SetImage(5)  dragon_windup.png 

-- Add prop to be the renderable for this character
entity:setProp(prop, layer)
-- Start the character (allow calls to OnUpdate)
entity:start()
entity:setLoc(-400, -200)
entity:setRot(0)
entity:setLinearVel(10, 20)
entity:setAngularVel(30)

-- FANTASMA ENEMY
texture_name_ghost = "sample/dragon.png"
gfxQuadGhost = MOAIGfxQuad2D.new()
gfxQuadGhost:setTexture(texture_name_ghost)
char_size_ghost = 64
gfxQuadGhost:setRect(-char_size_ghost/2, -char_size_ghost/2, char_size_ghost/2, char_size_ghost/2)
gfxQuadGhost:setUVRect(0, 0, 1, 1)
  
propGhost = MOAIProp2D.new()
propGhost:setDeck(gfxQuadGhost)

entityGhost = Enemy.new()

-- Add prop to be the renderable for this enemy character
entityGhost:addImage(idle)   -- SetImage(0)  dragon_idle.png
entityGhost:addImage(alarm)  -- SetImage(1)  dragon_alarm.png
entityGhost:addImage(attack) -- SetImage(2)  dragon_attack.png
entityGhost:addImage(dead)   -- SetImage(3)  dragon_dead.png
entityGhost:addImage(hit)    -- SetImage(4)  dragon_hit.png
entityGhost:addImage(windup) -- SetImage(5)  dragon_windup.png

-- Add prop to be the renderable for this character
entityGhost:setProp(propGhost, layer)
-- Start the character (allow calls to OnUpdate)
entityGhost:start()
entityGhost:setLoc(-150, -200)
entityGhost:setRot(0)
entityGhost:setLinearVel(10, 20)
entityGhost:setAngularVel(30)
entityGhost:setTarget(entity) 

-- Enable Debug Draw
debug = MOAIDrawDebug.get();
layer:setDrawDebug(debug)
-- Add this character to draw debug
MOAIDrawDebug.insertEntity(entity)
MOAIDrawDebug.insertEntity(entityGhost)

-- Create pathfinder
pathfinder = Pathfinder.new()
pathfinder:addCharacter(entity)
pathfinder:setStartPosition(-435, -204)
pathfinder:setEndPosition(-440, -130)
MOAIDrawDebug.insertEntity(pathfinder)

mouseX = 0
mouseY = 0

function onClick(down)
  pathfinder:setStartPosition(mouseX, mouseY)
  entity:setLoc(mouseX, mouseY)
  entity:setRot(-135)
end

function onRightClick(down)
  pathfinder:setEndPosition(mouseX, mouseY)
end

function pointerCallback(x, y)
    mouseX, mouseY = layer:wndToWorld(x, y)
end

MOAIInputMgr.device.mouseLeft:setCallback(onClick)
MOAIInputMgr.device.mouseRight:setCallback(onRightClick)
MOAIInputMgr.device.pointer:setCallback(pointerCallback)

function onKeyPressed(key, down)
	if key == 32 then
		if down then
			print(tostring(key))
		else
			pathfinder:pathfindStep()
		end
	end
end

if (MOAIInputMgr.device.keyboard) then
    MOAIInputMgr.device.keyboard:setCallback(onKeyPressed)
end
