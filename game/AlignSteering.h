#pragma once

#include "uslscore\USVec2D.h"

class Character;

class AlignSteering {
public:
	AlignSteering() {}
	AlignSteering(Character *character, float target);
	float GetSteering(); // Calculo de la nueva aceleracion.
	void DrawDebug();

	Character *mCharacter;
	float    mTarget;
	float    mAcce;
	float	 mDreamVelocity;
};