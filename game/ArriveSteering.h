#pragma once

#include "uslscore\USVec2D.h"

class Character;

class ArriveSteering {
public:
	ArriveSteering() {}
	ArriveSteering(Character *character, USVec2D target);
	USVec2D GetSteering(); // Calculo de la nueva aceleracion.
	void DrawDebug();

	Character *mCharacter;
	USVec2D    mTarget;
	USVec2D    mAcce;
	USVec2D	   mDreamVelocity;
};