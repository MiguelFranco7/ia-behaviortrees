#include "stdafx.h"
#include "PursueSteering.h"
#include "character.h"

PursueSteering::PursueSteering(Character * character, Character *target) {
	mCharacter = character;
	mTarget = target;
}

USVec2D PursueSteering::GetSteering() {
	if (mTarget != nullptr) {
		USVec2D characterLoc = mCharacter->GetLoc();
		USVec2D targetLoc = mTarget->GetLoc();

		//float dist = targetLoc.Dist(characterLoc);
		USVec2D distance = targetLoc - characterLoc;
		int ahead = distance.Length() / mCharacter->GetParams().max_velocity;
		USVec2D futurePosition = targetLoc + mTarget->GetLinearVelocity() * ahead;

		mCharacter->SetTarget(futurePosition);

		// ARRIVE
		ArriveSteering arrive = ArriveSteering(mCharacter, futurePosition);
		mAcce = arrive.GetSteering();

		// SEEK
		/*SeekSteering seek = SeekSteering(mCharacter, futurePosition);
		mAcce = seek.GetSteering();*/
	} else {
		mAcce.mX = 0;
		mAcce.mY = 0;
	}

	return mAcce;
}

void PursueSteering::DrawDebug() {
	MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
	gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);

	// Vector (L�nea) de la velocidad deseada calculada en el frame anterior
	//MOAIDraw::DrawLine(USVec2D(mCharacter->GetLoc().mX, mCharacter->GetLoc().mY), mDreamVelocity);

	gfxDevice.SetPenColor(0.0f, 1.0f, 0.0f, 0.5f);
	// Vector (L�nea) de la aceleraci�n calculada en el frame anterior 
	MOAIDraw::DrawLine(USVec2D(mCharacter->GetLoc().mX, mCharacter->GetLoc().mY), mAcce);
}
