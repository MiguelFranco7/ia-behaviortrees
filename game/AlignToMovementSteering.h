#pragma once

#include "AlignSteering.h"

class Character;

class AlignToMovementSteering {
public:
	AlignToMovementSteering () { mSteering = AlignSteering(); }
	AlignToMovementSteering(Character *character);
	float GetSteering();
	void DrawDebug();

	Character    *mCharacter;
	float         mTarget;
	float         mAcce;
	float	      mDreamVelocity;
	AlignSteering mSteering;
};