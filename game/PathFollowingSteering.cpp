#include "stdafx.h"
#include "PathFollowingSteering.h"
#include "character.h"

PathFollowingSteering::PathFollowingSteering(Character *character, vector<USVec2D> path) {
	mCharacter = character;
	mPath = path;
	mCurrentNode = 0;
}

USVec2D PathFollowingSteering::GetSteering() {
	if (mPath.size() > 0) {
		// Calcular la posicion en el path mas cercana
		mNearPoint = FindNearPointToSegment();

		// Calcular una posicion en path mas avanzada
		USVec2D direction = mPath[mCurrentNode + 1] - mPath[mCurrentNode];
		direction.SetLength(mCharacter->GetParams().lookAhead);
		mNextPoint = mNearPoint + direction;

		mCharacter->SetTarget(mNextPoint);

		// ARRIVE
		mArrive = ArriveSteering(mCharacter, mNextPoint);
		mAcce = mArrive.GetSteering();
	}
	else
		mAcce = USVec2D();

	return mAcce;
}

void PathFollowingSteering::DrawDebug() {
	if (mPath.size() > 0) {
		MOAIGfxDevice& gfxDevice = MOAIGfxDevice::Get();
		gfxDevice.SetPenColor(1.0f, 0.0f, 0.0f, 0.5f);
		MOAIDraw::DrawEllipseFill(mNearPoint.mX, mNearPoint.mY, 5, 5, 100);

		gfxDevice.SetPenColor(0.0f, 1.0f, 0.0f, 0.5f);
		MOAIDraw::DrawPoint(mNextPoint.mX, mNextPoint.mY);
		MOAIDraw::DrawEllipseFill(mNextPoint.mX, mNextPoint.mY, 5, 5, 100);

		//mArrive.DrawDebug();
	}
}

USVec2D PathFollowingSteering::FindNearPointToSegment() {
	USVec2D from  = mPath[mCurrentNode];
	USVec2D to    = mPath[mCurrentNode + 1];
	USVec2D point = mCharacter->GetLoc();
	float diffX;
	float diffY;
	float minDistance = INFINITY;
	USVec2D nearPoint;

	for (int i = mCurrentNode; i < mPath.size() - 1; i++) {
		from = mPath[i];
		to   = mPath[i + 1];

		diffX = to.mX - from.mX;
		diffY = to.mY - from.mY;

		if ((diffX == 0) && (diffY == 0)) {
			diffX = point.mX - from.mX;
			diffY = point.mY - from.mY;
			return USVec2D(diffX, diffY);
		}

		float t = ((point.mX - from.mX) * diffX + (point.mY - from.mY) * diffY) / (diffX * diffX + diffY * diffY);

		if (t < 0.f) {
			// Point is nearest to the first point i.e x1 and y1
			diffX = point.mX - from.mX;
			diffY = point.mY - from.mY;

			if (USVec2D(point.mX + diffX, point.mY + diffY).Length() <= minDistance) {
				minDistance = USVec2D(point.mX + diffX, point.mY + diffY).Length();
				mCurrentNode = i;
				nearPoint = USVec2D(point.mX - diffX, point.mY - diffY);
			}
		}
		else if (t > 1.f) {
			// Point is nearest to the end point i.e x2 and y2
			diffX = point.mX - to.mX;
			diffY = point.mY - to.mY;

			if (USVec2D(point.mX + diffX, point.mY + diffY).Length() <= minDistance) {
				minDistance = USVec2D(point.mX + diffX, point.mY + diffY).Length();
				if (i + 1 < mPath.size() - 1)
					mCurrentNode = i + 1;
				else
					mCurrentNode = i;
				nearPoint = USVec2D(point.mX - diffX, point.mY - diffY);
			}
		}
		else {
			// If perpendicular line intersect the line segment.
			diffX = point.mX - (from.mX + t * diffX);
			diffY = point.mY - (from.mY + t * diffY);

			if (USVec2D(point.mX + diffX, point.mY + diffY).Length() <= minDistance) {
				minDistance  = USVec2D(point.mX + diffX, point.mY + diffY).Length();
				mCurrentNode = i;
				nearPoint    = USVec2D(point.mX - diffX, point.mY - diffY);
			}
		}
	}

	// Returning shortest point
	return nearPoint;
}
