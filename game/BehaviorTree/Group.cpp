#include "stdafx.h"
#include "Group.h"
#include "BehaviorTree\Sequence.h"
#include "BehaviorTree\Selector.h"
#include "BehaviorTree\Behaviors\Actions\AlarmBehavior.h"
#include "BehaviorTree\Behaviors\Actions\AttackBehavior.h"
#include "BehaviorTree\Behaviors\Actions\DeadBehavior.h"
#include "BehaviorTree\Behaviors\Actions\HitBehavior.h"
#include "BehaviorTree\Behaviors\Actions\IdleBehavior.h"
#include "BehaviorTree\Behaviors\Actions\MoveEnemyBehavior.h"
#include "BehaviorTree\Behaviors\Actions\MovePlayerBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\CanAttackConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\CanSeePlayerConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\ContinueAttackConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\ContinueHitConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\EnemyKillPlayerConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\IsAliveConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\PlayerIsNearConditionBehavior.h"
#include "BehaviorTree\Behaviors\Conditions\ReceiveHitConditionBehavior.h"
#include "Enemy.h"
#include "Player.h"
#include <tinyxml.h>

Group::~Group() {
	for (int i = mChildren.size() - 1; i >= 0; --i)
		delete mChildren[i];

	mChildren.clear();
}

void Group::AddBehavior(BehaviorTree * behavior) {
	mChildren.push_back(behavior);
}

bool Group::load(const char * filename, Character * owner) {
	TiXmlDocument doc(filename);
	if (!doc.LoadFile())
	{
		fprintf(stderr, "Couldn't read params from %s", filename);
		return false;
	}

	TiXmlHandle hDoc(&doc);

	TiXmlElement* pElem;
	pElem = hDoc.FirstChildElement().Element();
	if (!pElem)
	{
		fprintf(stderr, "Invalid format for %s", filename);
		return false;
	}

	TiXmlHandle hRoot(pElem);
	TiXmlElement * behaviorElem = hRoot.FirstChild("Behavior").Element();
	for (behaviorElem; behaviorElem; behaviorElem = behaviorElem->NextSiblingElement()) {
		const char * behaviorType = behaviorElem->Attribute("type");

		if (!strcmp(behaviorType, "Sequence")) {
			Sequence * sequence = static_cast<Sequence *>(loadGroup(behaviorElem, owner));

			AddBehavior(sequence);
		}
		else if (!strcmp(behaviorType, "Selector")) {
			Selector * selector = static_cast<Selector *>(loadGroup(behaviorElem, owner));

			AddBehavior(selector);
		}
		else if (!strcmp(behaviorType, "Condition")) {
			BehaviorTree * condition = loadCondition(behaviorElem, owner);

			AddBehavior(condition);
		}
		else if (!strcmp(behaviorType, "Action")) {
			BehaviorTree * action = loadAction(behaviorElem, owner);

			AddBehavior(action);
		}
	}

	return true;
}

Group * Group::loadGroup(TiXmlElement * groupElem, Character * owner) {
	Group * group;
	const char * behaviorType = groupElem->Attribute("type");

	if (!strcmp(behaviorType, "Sequence")) {
		group = new Sequence();
	}
	else if (!strcmp(behaviorType, "Selector")) {
		group = new Selector();
	}

	groupElem->FirstChild("Behavior")->FirstChildElement();

	groupElem = groupElem->FirstChild("Behavior")->ToElement();
	for (groupElem; groupElem; groupElem = groupElem->NextSiblingElement()) {
		const char * behaviorType = groupElem->Attribute("type");

		if (!strcmp(behaviorType, "Sequence")) {
			Sequence * sequence = static_cast<Sequence *>(loadGroup(groupElem, owner));

			group->AddBehavior(sequence);
		}
		else if (!strcmp(behaviorType, "Selector")) {
			Selector * selector = static_cast<Selector *>(loadGroup(groupElem, owner));

			group->AddBehavior(selector);
		}
		else if (!strcmp(behaviorType, "Condition")) {
			BehaviorTree * condition = loadCondition(groupElem, owner);

			group->AddBehavior(condition);
		}
		else if (!strcmp(behaviorType, "Action")) {
			BehaviorTree * action = loadAction(groupElem, owner);

			group->AddBehavior(action);
		}
	}

	return group;
}

BehaviorTree * Group::loadCondition(TiXmlElement * conditionElem, Character * owner) {
	const char * behaviorFunction = conditionElem->Attribute("function");

	if (!strcmp(behaviorFunction, "EnemyKillPlayer")) {
		EnemyKillPlayerConditionnBehavior * enemyKillPlayerConditionnBehavior = new EnemyKillPlayerConditionnBehavior(static_cast<Enemy *>(owner));

		return enemyKillPlayerConditionnBehavior;
	}
	else if (!strcmp(behaviorFunction, "CanAttack")) {
		float distanceAttack;
		conditionElem->Attribute("distanceAttack", &distanceAttack);
		CanAttackConditionBehavior * canAttackConditionBehavior = new CanAttackConditionBehavior(static_cast<Enemy *>(owner), distanceAttack);

		return canAttackConditionBehavior;
	}
	else if (!strcmp(behaviorFunction, "CanSeePlayer")) {
		float distanceSee;
		conditionElem->Attribute("distanceSee", &distanceSee);
		CanSeePlayerConditionBehavior * canSeePlayerConditionBehavior = new CanSeePlayerConditionBehavior(static_cast<Enemy *>(owner), distanceSee);

		return canSeePlayerConditionBehavior;
	}
	else if (!strcmp(behaviorFunction, "PlayerIsNear")) {
		float playerIsNear;
		conditionElem->Attribute("distanceNear", &playerIsNear);
		PlayerIsNearConditionBehavior * playerIsNearConditionBehavior = new PlayerIsNearConditionBehavior(static_cast<Enemy *>(owner), playerIsNear);

		return playerIsNearConditionBehavior;
	}
	else if (!strcmp(behaviorFunction, "ContinueHit")) {
		ContinueHitConditionBehavior * continueHitCondition = new ContinueHitConditionBehavior(static_cast<Player *>(owner));

		return continueHitCondition;
	}
	else if (!strcmp(behaviorFunction, "IsAlive")) {
		IsAliveConditionBehavior * isAliveCondition = new IsAliveConditionBehavior(owner);

		return isAliveCondition;
	}

	return nullptr;
}

BehaviorTree * Group::loadAction(TiXmlElement * actionElem, Character * owner) {
	const char * behaviorFunction = actionElem->Attribute("function");

	int indexImage;
	actionElem->Attribute("indexImage", &indexImage);

	if (!strcmp(behaviorFunction, "Attack")) {
		float attackDuration;
		actionElem->Attribute("attackDuration", &attackDuration);
		float damage;
		actionElem->Attribute("damage", &damage);
		AttackBehavior * attack = new AttackBehavior(static_cast<Enemy *>(owner), indexImage, attackDuration, damage);

		return attack;
	}
	else if (!strcmp(behaviorFunction, "MoveEnemy")) {
		MoveEnemyBehavior * moveEnemyBehavior = new MoveEnemyBehavior(static_cast<Enemy *>(owner), indexImage);

		return moveEnemyBehavior;
	}
	else if (!strcmp(behaviorFunction, "Alarm")) {
		AlarmBehavior * alarmBehavior = new AlarmBehavior(static_cast<Enemy *>(owner), indexImage);

		return alarmBehavior;
	}
	else if (!strcmp(behaviorFunction, "Idle")) {
		IdleBehavior * idleBehavior = new IdleBehavior(static_cast<Enemy *>(owner), indexImage);

		return idleBehavior;
	}
	else if (!strcmp(behaviorFunction, "Dead")) {
		DeadBehavior * deadBehavior = new DeadBehavior(owner, indexImage);

		return deadBehavior;
	}
	else if (!strcmp(behaviorFunction, "Hit")) {
		float hitDuration;
		actionElem->Attribute("hitDuration", &hitDuration);
		HitBehavior * hitBehavior = new HitBehavior(static_cast<Player *>(owner), indexImage, hitDuration);

		return hitBehavior;
	}
	else if (!strcmp(behaviorFunction, "MovePlayer")) {
		MovePlayerBehavior * movePlayerBehavior = new MovePlayerBehavior(static_cast<Player *>(owner), indexImage);

		return movePlayerBehavior;
	}

	return nullptr;
}
