#include "stdafx.h"
#include "BehaviorTree.h"

BehaviorTree::BehaviorTree() {
	mStatus    = eInvalid;
	mNeedStart = true;
}

Status BehaviorTree::tick() {
	if (mNeedStart) {
		onEnter();
		mNeedStart = false;
	}

	mStatus = update();

	if (mStatus != eRunning) {
		onExit();
		mNeedStart = true;
	}

	return mStatus;
}
