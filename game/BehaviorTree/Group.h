#pragma once
#include "BehaviorTree.h"
#include <vector>

class TiXmlElement;
class Character;

class Group : public BehaviorTree {
public:
	~Group();
	void AddBehavior(BehaviorTree * behavior);

	bool load(const char * filename, Character * owner);

protected:
	typedef std::vector<BehaviorTree *> Behaviors;
	Behaviors mChildren;

private:
	Group * loadGroup(TiXmlElement * groupElem, Character * owner);
	BehaviorTree * loadCondition(TiXmlElement * conditionElem, Character * owner);
	BehaviorTree * loadAction(TiXmlElement * actionElem, Character * owner);
};
