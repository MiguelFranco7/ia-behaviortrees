#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Enemy;

class CanSeePlayerConditionBehavior : public BehaviorTree {
public:
	CanSeePlayerConditionBehavior(Enemy * owner, float distanceSeePlayer);

protected:
	virtual Status update();

private:
	Enemy * mOwner;
	float   mDistanceSeePlayer;
};
