#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Enemy;

class PlayerIsNearConditionBehavior : public BehaviorTree {
public:
	PlayerIsNearConditionBehavior(Enemy * owner, float distanceToAlert);

protected:
	virtual Status update();

private:
	Enemy * mOwner;
	float   mDistanceToAlert;
};
