#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Character;

class IsAliveConditionBehavior : public BehaviorTree {
public:
	IsAliveConditionBehavior(Character * owner);

protected:
	virtual Status update();

private:
	Character * mOwner;
};
