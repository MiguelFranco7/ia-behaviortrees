#include "stdafx.h"
#include "CanAttackConditionBehavior.h"
#include "Enemy.h"

CanAttackConditionBehavior::CanAttackConditionBehavior(Enemy * owner, float distanceToAttack) {
	mOwner			  = owner;
	mDistanceToAttack = distanceToAttack;
}

Status CanAttackConditionBehavior::update() {
	if (!mOwner->GetPlayerTarget())
		return eFail;

	if (mOwner->GetPlayerTarget()->GetCurrentHealth() <= 0.f)
		return eFail;

	USVec3D distanceVec = mOwner->GetLoc() - mOwner->GetPlayerTarget()->GetLoc();
	float distance = distanceVec.Length();

	if (distance < mDistanceToAttack)
		return eSuccess;

	return eFail;
}
