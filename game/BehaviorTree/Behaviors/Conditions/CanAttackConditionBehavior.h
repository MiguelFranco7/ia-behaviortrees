#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Enemy;

class CanAttackConditionBehavior : public BehaviorTree {
public:
	CanAttackConditionBehavior(Enemy * owner, float distanceToAttack);

protected:
	virtual Status update();

private:
	Enemy * mOwner;
	float   mDistanceToAttack;
};
