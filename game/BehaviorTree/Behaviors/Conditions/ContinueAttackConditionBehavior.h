#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Enemy;

class ContinueAttackConditionBehavior : public BehaviorTree {
public:
	ContinueAttackConditionBehavior(Enemy * owner);

protected:
	virtual Status update();

private:
	Enemy * mOwner;
};
