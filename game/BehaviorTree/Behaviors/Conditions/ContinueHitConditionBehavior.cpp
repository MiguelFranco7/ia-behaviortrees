#include "stdafx.h"
#include "ContinueHitConditionBehavior.h"
#include "Player.h"

ContinueHitConditionBehavior::ContinueHitConditionBehavior(Player * owner) {
	mOwner = owner;
}

Status ContinueHitConditionBehavior::update() {
	if (!mOwner)
		return eFail;

	if (mOwner->GetIsHitting())
		return eSuccess;

	return eFail;
}
