#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Player;

class ReceiveHitConditionBehavior : public BehaviorTree {
public:
	ReceiveHitConditionBehavior(Player * owner);

protected:
	virtual Status update();

private:
	Player * mOwner;
};
