#include "stdafx.h"
#include "ReceiveHitConditionBehavior.h"
#include "Player.h"

ReceiveHitConditionBehavior::ReceiveHitConditionBehavior(Player * owner) {
	mOwner = owner;
}

Status ReceiveHitConditionBehavior::update() {
	if (!mOwner)
		return eFail;

	if (mOwner->GetIsHitting())
		return eSuccess;

	return eFail;
}
