#include "stdafx.h"
#include "PlayerIsNearConditionBehavior.h"
#include "Enemy.h"

PlayerIsNearConditionBehavior::PlayerIsNearConditionBehavior(Enemy * owner, float distanceToAlert) {
	mOwner			 = owner;
	mDistanceToAlert = distanceToAlert;
}

Status PlayerIsNearConditionBehavior::update() {
	if (!mOwner || !mOwner->GetPlayerTarget() || mOwner->GetPlayerTarget()->GetCurrentHealth() <= 0.f)
		return eFail;

	USVec3D distanceVec = mOwner->GetLoc() - mOwner->GetPlayerTarget()->GetLoc();
	float distance = distanceVec.Length();

	if (distance < mDistanceToAlert)
		return eSuccess;
	
	return eFail;
}
