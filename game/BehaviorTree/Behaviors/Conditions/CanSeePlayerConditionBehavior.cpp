#include "stdafx.h"
#include "CanSeePlayerConditionBehavior.h"
#include "Enemy.h"

CanSeePlayerConditionBehavior::CanSeePlayerConditionBehavior(Enemy * owner, float distanceSeePlayer) {
	mOwner			   = owner;
	mDistanceSeePlayer = distanceSeePlayer;
}

Status CanSeePlayerConditionBehavior::update() {
	if (!mOwner->GetPlayerTarget())
		return eFail;

	USVec3D distanceVec = mOwner->GetLoc() - mOwner->GetPlayerTarget()->GetLoc();
	float distance = distanceVec.Length();

	if (distance < mDistanceSeePlayer)
		return eSuccess;

	return eFail;
}
