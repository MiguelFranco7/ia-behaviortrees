#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Enemy;

class EnemyKillPlayerConditionnBehavior : public BehaviorTree {
public:
	EnemyKillPlayerConditionnBehavior(Enemy * owner);

protected:
	virtual Status update();

private:
	Enemy * mOwner;
};