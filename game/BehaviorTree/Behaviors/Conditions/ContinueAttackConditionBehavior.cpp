#include "stdafx.h"
#include "ContinueAttackConditionBehavior.h"
#include "Enemy.h"

ContinueAttackConditionBehavior::ContinueAttackConditionBehavior(Enemy * owner) {
	mOwner = owner;
}

Status ContinueAttackConditionBehavior::update() {
	if (!mOwner)
		return eFail;

	if (!mOwner->GetIsAttacking())
		return eSuccess;

	return eFail;
}
