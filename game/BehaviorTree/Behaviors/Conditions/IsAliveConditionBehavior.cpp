#include "stdafx.h"
#include "IsAliveConditionBehavior.h"
#include "character.h"

IsAliveConditionBehavior::IsAliveConditionBehavior(Character * owner) {
	mOwner = owner;
}

Status IsAliveConditionBehavior::update() {
	if (!mOwner)
		return eFail;

	if (mOwner->GetCurrentHealth() <= 0.f)
		return eSuccess;

	return eFail;
}
