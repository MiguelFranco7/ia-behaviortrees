#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Player;

class ContinueHitConditionBehavior : public BehaviorTree {
public:
	ContinueHitConditionBehavior(Player * owner);

protected:
	virtual Status update();

private:
	Player * mOwner;
};