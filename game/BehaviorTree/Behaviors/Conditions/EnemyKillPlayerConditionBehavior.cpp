#include "stdafx.h"
#include "EnemyKillPlayerConditionBehavior.h"
#include "Enemy.h"

EnemyKillPlayerConditionnBehavior::EnemyKillPlayerConditionnBehavior(Enemy * owner) {
	mOwner = owner;
}

Status EnemyKillPlayerConditionnBehavior::update() {
	if (!mOwner || !mOwner->GetPlayerTarget())
		return eFail;

	if (mOwner->GetPlayerTarget()->GetCurrentHealth() >= 0.f)
		return eSuccess;

	return eFail;
}
