#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Character;

class IdleBehavior : public BehaviorTree {
public:
	IdleBehavior(Character * owner, int indexImage);

protected:
	virtual Status update();
	virtual void   onEnter();
	virtual void   onExit();

private:
	Character * mOwner;
	int			mIndexImage;
};
