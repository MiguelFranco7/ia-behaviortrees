#include "stdafx.h"
#include "IdleBehavior.h"
#include "character.h"

IdleBehavior::IdleBehavior(Character * owner, int indexImage) {
	mOwner		= owner;
	mIndexImage = indexImage;
}

Status IdleBehavior::update() {
	if (!mOwner)
		eFail;

	return eSuccess;
}

void IdleBehavior::onEnter() {
	mOwner->SetImage(mIndexImage);
}

void IdleBehavior::onExit() {}
