#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Enemy;

class AlarmBehavior : public BehaviorTree {
public:
	AlarmBehavior(Enemy * owner, int indexImage);

protected:
	virtual Status update();
	virtual void   onEnter();
	virtual void   onExit();

private:
	Enemy * mOwner;
	int     mIndexImage;
};
