#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Enemy;

class MoveEnemyBehavior : public BehaviorTree {
public:
	MoveEnemyBehavior(Enemy * owner, int indexImage);

protected:
	virtual Status update();
	virtual void   onEnter();
	virtual void   onExit();

private:
	Enemy * mOwner;
	int     mIndexImage;
};
