#include "stdafx.h"
#include "DeadBehavior.h"
#include "character.h"

DeadBehavior::DeadBehavior(Character * owner, int indexImage) {
	mOwner		= owner;
	mIndexImage = indexImage;
}

Status DeadBehavior::update() {
	if (!mOwner)
		return eFail;

	return eSuccess;
}

void DeadBehavior::onEnter() {
	mOwner->SetImage(mIndexImage);
}

void DeadBehavior::onExit() {}
