#include "stdafx.h"
#include "AlarmBehavior.h"
#include "Enemy.h"

AlarmBehavior::AlarmBehavior(Enemy * owner, int indexImage) {
	mOwner		= owner;
	mIndexImage = indexImage;
}

Status AlarmBehavior::update()
{
	if (!mOwner)
		return eFail;

	return eSuccess;
}

void AlarmBehavior::onEnter() {
	mOwner->SetImage(mIndexImage);
}

void AlarmBehavior::onExit() {}
