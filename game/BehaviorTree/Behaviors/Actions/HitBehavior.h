#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Player;

class HitBehavior : public BehaviorTree {
public:
	HitBehavior(Player * owner, int indexImage, float hitDuration);

protected:
	virtual Status update();
	virtual void   onEnter();
	virtual void   onExit();

private:
	Player *	mOwner;
	int         mIndexImage;
	float		mHitDuration;
	float		mCurrentHitDuration;
};
