#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Character;

class DeadBehavior : public BehaviorTree {
public:
	DeadBehavior(Character * owner, int indexImage);

protected:
	virtual Status update();
	virtual void   onEnter();
	virtual void   onExit();

private:
	Character * mOwner;
	int		    mIndexImage;
};
