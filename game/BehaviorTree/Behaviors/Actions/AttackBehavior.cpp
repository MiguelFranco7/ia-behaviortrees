#include "stdafx.h"
#include "AttackBehavior.h"
#include "Enemy.h"

AttackBehavior::AttackBehavior(Enemy * owner, int indexImage, float attackDuration, float damageAttack) {
	mOwner			= owner;
	mIndexImage     = indexImage;
	mAttackDuration = attackDuration;
	mDamageAttack   = damageAttack;
}

Status AttackBehavior::update() {
	if (!mOwner)
		return eFail;

	mCurrentAttackDuration += mOwner->GetCurrentDeltaTime();

	if (mCurrentAttackDuration >= mAttackDuration) {
		mOwner->SetIsAttacking(false);

		return eSuccess;
	}

	return eRunning;
}

void AttackBehavior::onEnter() {
	mOwner->SetImage(mIndexImage);

	mCurrentAttackDuration = 0.f;
	mOwner->SetIsAttacking(true);
	mOwner->GetPlayerTarget()->OnDamage(mDamageAttack);
}

void AttackBehavior::onExit() {}
