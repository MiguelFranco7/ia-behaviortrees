#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Player;

class MovePlayerBehavior : public BehaviorTree {
public:
	MovePlayerBehavior(Player * owner, int indexImage);

protected:
	virtual Status update();
	virtual void   onEnter();
	virtual void   onExit();

private:
	Player * mOwner;
	int      mIndexImage;
};
