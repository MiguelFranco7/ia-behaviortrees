#include "stdafx.h"
#include "MovePlayerBehavior.h"
#include "Player.h"

MovePlayerBehavior::MovePlayerBehavior(Player * owner, int indexImage) {
	mOwner		= owner;
	mIndexImage = indexImage;
}

Status MovePlayerBehavior::update() {
	if (!mOwner)
		return eFail;

	return eSuccess;
}

void MovePlayerBehavior::onEnter() {
	mOwner->SetImage(mIndexImage);
}

void MovePlayerBehavior::onExit() {}
