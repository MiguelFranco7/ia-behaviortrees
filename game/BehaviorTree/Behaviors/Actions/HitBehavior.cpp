#include "stdafx.h"
#include "HitBehavior.h"
#include "Player.h"

HitBehavior::HitBehavior(Player * owner, int indexImage, float hitDuration) {
	mOwner = owner;
	mIndexImage = indexImage;
	mHitDuration = hitDuration;
}

Status HitBehavior::update() {
	if (!mOwner)
		return eFail;

	mCurrentHitDuration += mOwner->GetCurrentDeltaTime();

	if (mCurrentHitDuration >= mHitDuration) {
		mOwner->SetIsHitting(false);

		return eSuccess;
	}

	return eRunning;
}

void HitBehavior::onEnter() {
	mOwner->SetImage(mIndexImage);

	mCurrentHitDuration = 0.f;
	mOwner->SetIsHitting(true);
}

void HitBehavior::onExit() {}
