#include "stdafx.h"
#include "MoveEnemyBehavior.h"
#include "Enemy.h"

MoveEnemyBehavior::MoveEnemyBehavior(Enemy * owner, int indexImage) {
	mOwner		= owner;
	mIndexImage = indexImage;
}

Status MoveEnemyBehavior::update() {
	if (!mOwner)
		return eFail;

	if (mOwner->GetPlayerTarget() && mOwner->GetPlayerTarget()->GetCurrentHealth() > 0.f)
		mOwner->MoveToPlayer();

	return eSuccess;
}

void MoveEnemyBehavior::onEnter() {
	mOwner->SetImage(mIndexImage);
}

void MoveEnemyBehavior::onExit() {}
