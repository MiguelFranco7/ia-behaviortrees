#pragma once
#include "BehaviorTree\BehaviorTree.h"

class Enemy;

class AttackBehavior : public BehaviorTree {
public:
	AttackBehavior(Enemy * owner, int indexImage, float attackDuration, float damageAttack);

protected:
	virtual Status update();
	virtual void   onEnter();
	virtual void   onExit();

private:
	Enemy * mOwner;
	int     mIndexImage;
	float	mAttackDuration;
	float	mCurrentAttackDuration;
	float	mDamageAttack;
};
