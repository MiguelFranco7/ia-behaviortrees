#include "stdafx.h"
#include "Sequence.h"

void Sequence::onEnter() {
	mCurrentChild = 0;
}

Status Sequence::update() {
	while (true) {
		Status status = mChildren[mCurrentChild]->tick();

		if (status != eSuccess)
			return status;

		++mCurrentChild;

		if (mCurrentChild == mChildren.size()) {
			mCurrentChild = 0;
			return eSuccess;
		}
	}

	return eInvalid;
}
