#include "stdafx.h"
#include "Selector.h"

void Selector::onEnter() {
	mCurrentChild = 0;
}

Status Selector::update() {
	while (true) {
		Status status = mChildren[mCurrentChild]->tick();

		if (status != eFail) {
			mCurrentChild = 0;
			return status;
		}

		++mCurrentChild;

		if (mCurrentChild == mChildren.size()) {
			mCurrentChild = 0;
			return eFail;
		}
	}
}
