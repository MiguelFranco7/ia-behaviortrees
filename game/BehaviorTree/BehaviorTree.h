#pragma once

class Group;

enum Status {
	eInvalid,
	eSuccess,
	eFail,
	eRunning
};

class BehaviorTree {
public:
	BehaviorTree();
	Status tick();

protected:
	virtual Status update()  { return mStatus; }
	virtual void   onEnter() {}
	virtual void   onExit()  {}

	Status mStatus;
	bool   mNeedStart;
};
