#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include <moaicore/MOAIEntity2D.h>
#include <params.h>
#include "SeekSteering.h"
#include "ArriveSteering.h"
#include "AlignSteering.h"
#include "AlignToMovementSteering.h"
#include "PursueSteering.h"
#include "PathFollowingSteering.h"
#include "ObstacleAvoidanceSteering.h"
#include "BehaviorTree\BehaviorTree.h"

class StateMachine;
class Selector;

class Character: public MOAIEntity2D {
public:
    DECL_LUA_FACTORY(Character)

	Character();
	~Character();

	virtual void DrawDebug();
	virtual void UpdatePath(vector<USVec2D> pathfollowing);
	virtual void OnDamage(float damage);
	
	void SetLinearVelocity(float x, float y) { mLinearVelocity.mX = x; mLinearVelocity.mY = y;}
	void SetAngularVelocity(float angle) { mAngularVelocity = angle;}
	
	USVec2D			  GetLinearVelocity() const { return mLinearVelocity;}
	float			  GetAngularVelocity() const { return mAngularVelocity;}
	Params			  GetParams() const { return mParams; }
	USVec2D			  GetTarget() { return mTarget; }
	void			  SetTarget(USVec2D target) { mTarget = target; }
	vector<Obstacles> GetObstacles() { return mObstacles; }
	void              SetObstacleCollision(int pos, bool collision) { mObstacles[pos].collision = collision; }
	float			  GetCurrentHealth()    { return mCurrentHealth; }
	float			  GetCurrentDeltaTime() { return mDeltaTime; }

protected:
	virtual void OnStart();
	virtual void OnStop();
	virtual void OnUpdate(float step);

	virtual void InitStateMachine();
	virtual void InitBehaviorTree();

	USVec2D mLinearVelocity;
	float   mAngularVelocity;
	
	Params					  mParams;
	vector<USVec2D>			  mPath;
	vector<Obstacles>		  mObstacles;
	USVec2D					  mTarget;
	SeekSteering			  seek;
	ArriveSteering			  arrive;
	AlignSteering			  align;
	AlignToMovementSteering   alignToMovement;
	PursueSteering			  pursue;
	PathFollowingSteering	  pathFollowing;
	ObstacleAvoidanceSteering obstacleAvoidance;

	float mMaxHealth = 25.f;
	float mCurrentHealth = 0.f;
	float mDeltaTime;
	StateMachine * mStateMachine;
	Selector     * mBehaviorTree;


	// Lua configuration
public:
	virtual void RegisterLuaFuncs(MOAILuaState& state);
private:
	static int _setLinearVel(lua_State* L);
	static int _setAngularVel(lua_State* L);
	static int _setPathfinder(lua_State* L);
};

#endif
